<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

// This is for the SSL certificate validation.
Route::get('/.well-known/pki-validation/starfield.html', function () {
    return view('starfield');
});

// This is the the Recaptcha verification proxy.
Route::get('recaptcha-site-verify', 'RecaptchaController@index');

// These must not be authenticated.
Route::get('publish/page/{token}', 'PagesController@getPageByToken');
Route::post('user/request-password-reset', 'UsersController@requestPasswordReset');
Route::put('user/password', 'UsersController@updatePassword');
Route::post('contact/', 'ContactController@store');

// All these routes need to be authenticated.
Route::middleware(['auth:api'])->group(function () {
    // Pages endpoints
    Route::resource('pages', 'PagesController');
    Route::put('pages/{id}/move-up', 'PagesController@moveUp');
    Route::put('pages/{id}/move-down', 'PagesController@moveDown');

    // Panels endpoints
    Route::post('pages/{id}/panels', 'PanelsController@store');
    Route::get('pages/{id}/panels', 'PanelsController@index');
    Route::resource('panels', 'PanelsController', ['except' => ['store', 'index']]);
    Route::put('panels/{id}/move-up', 'PanelsController@moveUp');
    Route::put('panels/{id}/move-down', 'PanelsController@moveDown');

    // Bookmarks endpoints
    Route::post('panels/{id}/bookmarks', 'BookmarksController@store');
    Route::post('sections/{id}/bookmarks', 'SectionsController@storeBookmark');
    Route::post('cells/{id}/bookmarks', 'CellsController@storeBookmark');
    Route::resource('bookmarks', 'BookmarksController', ['except' => ['store', 'index']]);

    // Sections endpoints
    Route::post('panels/{id}/sections', 'SectionsController@storeSection');
    Route::resource('sections', 'SectionsController', ['except' => ['store', 'index']]);

    // Grids/Columns/Rows endpoints
    Route::post('sections/{id}/grids', 'GridsController@store');
    Route::post('grids/{id}/rows', 'RowsController@store');
    Route::post('grids/{id}/columns', 'ColumnsController@store');

    // Other functions...

    // User details
    Route::put('user', 'UsersController@update');
    Route::delete('user', 'UsersController@destroy');

    // Publish a page
    Route::post('publish/page/{id}', 'PublishController@publishPage');
    Route::get('publish/page/{id}/token', 'PublishController@getPageToken');
    Route::delete('publish/page/{id}', 'PublishController@unpublishPage');

    // Bookmark labels
    Route::get('labels', 'LabelsController@index');

    // Managing nodes
    Route::delete('nodes/{id}/{parentNodeId}', 'NodeController@delete'); // For non-page nodes.
    Route::delete('nodes/{id}', 'NodeController@delete'); // For page nodes.
    Route::get('nodes/{id}/hierarchies', 'NodeController@hierarchies');
    Route::get('nodes/sharedWith', 'NodeController@sharedWith'); // Which node(s) is/are shared with the user?
    Route::post('nodes/{id}/embed', 'NodeController@embed');
    Route::post('nodes/{id}/move', 'NodeController@move');

    // Groups and members.
    Route::get('user/loggedInUser', 'UsersController@loggedInUser');
    Route::get('user/groups', 'UsersController@groups'); // Which groups are the user in?
    Route::resource('groups', 'GroupsController');
    Route::get('groups/{id}/members', 'GroupsController@members');
    Route::post('groups/{id}/members', 'GroupsController@inviteMember');
    Route::delete('groups/{groupId}/members/{userId}', 'GroupsController@removeMember');
    Route::put('groups/{groupId}/members/{userId}', 'GroupsController@updateMember');
    Route::post('groups/{groupId}/members/{userId}', 'GroupsController@resendInvitation');
    Route::post('groups/{groupId}/nodes/{nodeId}', 'GroupsController@addNode');
    Route::delete('groups/{groupId}/nodes/{nodeId}', 'GroupsController@removeNode');
    Route::get('groups/{groupId}/nodes', 'GroupsController@nodes');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
