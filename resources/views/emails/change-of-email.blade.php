<?php
use App\Models\User;

/**
 * @var User $user
 * @var bool $toNewAddress
 */
?>
@include('emails.common.salutation', array('user' => $user))

<p>The email address for your bookmarks account has been changed.</p>

<p>This email is being sent to your {{ $toNewAddress ? 'new' : 'old'  }} address.</p>

<p>If you did not change your email address, @include('emails.common.contact-us-link')</p>

@include('emails.common.footer')
