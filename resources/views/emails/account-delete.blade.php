<?php
use App\Models\User;

/**
 * @var User $user
 */
?>
@include('emails.common.salutation', array('user' => $user))

<p>This is to confirm that your bookmarks account has been deleted.</p>

<p>Thank you for using this service. We hope to see you again one day!</p>

@include('emails.common.footer')
