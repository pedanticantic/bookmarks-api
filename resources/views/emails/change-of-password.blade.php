<?php
use App\Models\User;

/**
 * @var User $user
 */
?>
@include('emails.common.salutation', array('user' => $user))

<p>The password for your bookmarks account has been changed.</p>

<p>If you did not change your password, @include('emails.common.contact-us-link')</p>

@include('emails.common.footer')
