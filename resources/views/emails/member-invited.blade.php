<?php

use App\Models\Group;
use App\Models\User;

/**
 * @var User $newMember
 * @var Group $group
 * @var string managementUrl
 */
?>
@include('emails.common.salutation', array('user' => $newMember))

<p>You have been invited into the bookmarks group, "{{ $group->name }}".</p>

<p>Please click <a href="{{ $managementUrl  }}">here</a> to accept or reject the invitation. You may need to log in first.</p>

@include('emails.common.footer')
