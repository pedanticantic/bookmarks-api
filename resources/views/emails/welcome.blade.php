<?php
use App\Models\User;

/**
 * @var User $user
 */
?>
@include('emails.common.salutation', array('user' => $user))

<p>Welcome to bookmarks! Thank you for creating an account. We hope you find the application useful.</p>

<p>If you didn't register with us, @include('emails.common.contact-us-link')</p>

@include('emails.common.footer')
