<?php
use App\Models\User;

/**
 * @var User $user
 * @var string $resetPasswordUrl
 */
?>
@include('emails.common.salutation', array('user' => $user))

<p>You have made a request to reset your password.</p>

<p>Please click to <a href="{{ $resetPasswordUrl }}">reset your password</a>.</p>

<p>If you did not make this request, @include('emails.common.contact-us-link')</p>

@include('emails.common.footer')
