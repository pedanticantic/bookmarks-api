<?php
use App\Models\User;

/**
 * @var User $user
 */
?>
@include('emails.common.salutation', array('user' => $user))

<p>The profile for your bookmarks account has been changed (your given name or family name has changed).</p>

<p>If you did not change your profile, @include('emails.common.contact-us-link')</p>

@include('emails.common.footer')
