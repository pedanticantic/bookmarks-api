<?php
/**
 * @var string $fromAddress
 * @var string $contactSubject
 * @var string $details
 */
?>
<p>Someone has filled out the contact us form. Details below:</p>

<p>Email: {{ $fromAddress }}</p>

<p>Subject: {{ $contactSubject }}</p>

<p>Details: {{ $details }}</p>
