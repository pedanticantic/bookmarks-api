<?php

namespace App\Mail;

use App\Models\User;
use App\Services\Encryptor;
use Illuminate\Mail\Mailable;

class ResetPassword extends Mailable
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $resetPasswordUrl;

    public function __construct(User $user, string $resetPasswordUrl, int $expirySeconds = null)
    {
        $expirySeconds = $expirySeconds ?? 60*60*24; // Default to 24hrs
        $expiryTime = time()+$expirySeconds;
        $this->user = $user;
        $this->resetPasswordUrl = $resetPasswordUrl.'&t='.$expiryTime.'&u='.Encryptor::getForUser($user, $expiryTime);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.reset-password')
            ->subject('Reset password request')
            ->with([
                'user' => $this->user,
                'resetPasswordUrl' => $this->resetPasswordUrl,
                'contactUsUrl' => MailUrlHelper::buildContactUsUrl($this->user, 'didnotrequestpasswordreset'),
            ]);
    }
}