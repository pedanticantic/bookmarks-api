<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Mail\Mailable;

class EmailChanged extends Mailable
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var bool
     */
    private $toNewAddress;

    public function __construct(User $user, bool $toNewAddress)
    {
        $this->user = $user;
        $this->toNewAddress = $toNewAddress;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.change-of-email')
            ->subject('Your email address has been changed')
            ->with([
                'user' => $this->user,
                'toNewAddress' => $this->toNewAddress,
                'contactUsUrl' => MailUrlHelper::buildContactUsUrl($this->user, 'didnotchangeemail'),
            ]);
    }
}