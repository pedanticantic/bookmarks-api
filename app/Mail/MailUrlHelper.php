<?php

namespace App\Mail;

use App\Models\User;

class MailUrlHelper
{
    public static function buildContactUsUrl(User $user, string $subject)
    {
        return env('CLIENT_HOST').'/'.env('CONTACT_US').'?email='.$user->email.'&subject='.$subject;
    }
}