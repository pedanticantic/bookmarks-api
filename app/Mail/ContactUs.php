<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class ContactUs extends Mailable
{
    /**
     * @var string $fromAddress
     * @var string $contactSubject
     * @var string $details
     */
    private $fromAddress;
    private $contactSubject;
    private $details;

    public function __construct(string $fromAddress, string $subject, string $details)
    {
        $this->fromAddress = $fromAddress;
        $this->contactSubject = $subject;
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.contact-us')
            ->subject('A contact from bookmarks')
            ->with([
                'fromAddress' => $this->fromAddress,
                'contactSubject' => $this->contactSubject,
                'details' => $this->details,
            ]);
    }
}