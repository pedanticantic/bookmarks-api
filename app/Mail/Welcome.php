<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Mail\Mailable;

class Welcome extends Mailable
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.welcome')
            ->subject('Welcome to bookmarks')
            ->with([
                'user' => $this->user,
                'contactUsUrl' => MailUrlHelper::buildContactUsUrl($this->user, 'didnotregister'),
            ]);
    }
}