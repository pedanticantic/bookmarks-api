<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Mail\Mailable;

class AccountDeleted extends Mailable
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.account-delete')
            ->subject('Goodbye from bookmarks')
            ->with([
                'user' => $this->user,
            ]);
    }
}