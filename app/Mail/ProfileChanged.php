<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Mail\Mailable;

class ProfileChanged extends Mailable
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.change-of-profile')
            ->subject('Your profile has been updated')
            ->with([
                'user' => $this->user,
                'contactUsUrl' => MailUrlHelper::buildContactUsUrl($this->user, 'didnotchangeprofile'),
            ]);
    }
}