<?php

namespace App\Mail;

use App\Models\Group;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MemberInvited extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    private $newMember;

    /**
     * @var Group
     */
    private $group;

    /**
     * @var string
     */
    private $managementUrl;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param Group $group
     * @param string $managementUrl
     */
    public function __construct(User $user, Group $group, string $managementUrl)
    {
        $this->newMember = $user;
        $this->group = $group;
        $this->managementUrl = $managementUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.member-invited')
            ->subject('Invitation to a Bookmarks group')
            ->with([
                'newMember' => $this->newMember,
                'group' => $this->group,
                'managementUrl' => $this->managementUrl,
            ]);
    }
}
