<?php

namespace App\Models;

class Page extends AbstractElement
{
    const NODE_TYPE = 'Page';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];
}
