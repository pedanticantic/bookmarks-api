<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $groupId
 * @property int $nodeId
 */
class GroupsNode extends Model
{
    public function group()
    {
        return $this->hasOne(Group::class, 'id', 'groupId');
    }

    public function node()
    {
        return $this->hasOne(Node::class, 'id', 'nodeId');
    }
}