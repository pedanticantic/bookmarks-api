<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $userId
 * @property int $parentNodeId
 * @property int $childNodeId
 * @property int $sequenceNumber
 * @property Node parentNode
 * @property Node childNode
 */
class NodeParentToChild extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'node_parent_to_child';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['userId', 'parentNodeId', 'childNodeId', 'sequenceNumber'];

    public function parentNode()
    {
        return $this->hasOne(Node::class, 'id', 'parentNodeId');
    }

    public function childNode()
    {
        return $this->hasOne(Node::class, 'id', 'childNodeId');
    }
}