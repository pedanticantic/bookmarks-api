<?php

namespace App\Models;

class Grid extends AbstractElement
{
    const NODE_TYPE = 'Grid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
}
