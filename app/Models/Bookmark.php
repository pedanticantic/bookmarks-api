<?php

namespace App\Models;

/**
 * @property int id
 * @property int nodeId
 * @property string label
 * @property string url
 */
class Bookmark extends AbstractElement
{
    const NODE_TYPE = 'Bookmark';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['label', 'url'];

    // Always include the bookmark's labels
    protected $with = [
        'labels'
    ];

    public function labels()
    {
        return $this->hasManyThrough(Label::class, BookmarksLabel::class, 'bookmarkId', 'id', 'id', 'labelId');
    }
}
