<?php

namespace App\Models\ValueObjects;

use App\Models\LinksAndChildNode;

class ManageNodeInfo
{
    /**
     * @var LinksAndChildNode
     */
    private $nodeToManage;
    /**
     * @var LinksAndChildNode
     */
    private $newParentNode;
    /**
     * @var int
     */
    private $newChildIndex;

    public function __construct(LinksAndChildNode $nodeToManage, LinksAndChildNode $newParentNode, int $newChildIndex)
    {
        $this->nodeToManage = $nodeToManage;
        $this->newParentNode = $newParentNode;
        $this->newChildIndex = $newChildIndex;
    }

    public function getNodeToManage(): LinksAndChildNode
    {
        return $this->nodeToManage;
    }

    public function getNewParentNode(): LinksAndChildNode
    {
        return $this->newParentNode;
    }

    public function getNewChildIndex(): int
    {
        return $this->newChildIndex;
    }
}