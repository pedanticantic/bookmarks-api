<?php

namespace App\Models;

class Row extends AbstractElement
{
    const NODE_TYPE = 'Row';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
}
