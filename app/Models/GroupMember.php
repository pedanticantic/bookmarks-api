<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $groupId
 * @property int $userId
 * @property string $role
 * @property string $status
 */
class GroupMember extends Model
{
    const ROLE_ADMIN = 'Admin';
    const ROLE_MEMBER = 'Member';

    const STATUS_INVITED = 'Invited';
    const STATUS_ACCEPTED = 'Accepted';
    const STATUS_DECLINED = 'Declined';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'groups_members';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'userId');
    }

    public function group()
    {
        return $this->hasOne(Group::class, 'id', 'groupId');
    }
}