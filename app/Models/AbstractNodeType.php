<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property $nodeType
 * @property Page $page
 * @property Panel $panel
 * @property Bookmark $bookmark
 * @property Section $section
 * @property Grid $grid
 * @property Row $row
 * @property Cell $cell
 * @property User $owner
 */
abstract class AbstractNodeType extends Model
{
    public static function buildWithForAssocRows(array $otherWiths, string $parent = null)
    {
        $assocTableNames = ['page', 'panel', 'bookmark', 'section', 'grid', 'row', 'cell'];
        if ($parent) {
            $assocTableNames = array_map(
                function($tableName) use ($parent) {
                    return $parent.'.'.$tableName;
                },
                $assocTableNames
            );
        }

        return array_merge($otherWiths, $assocTableNames);
    }

    public function page()
    {
        return $this->hasOne(Page::class, 'nodeId', 'id');
    }

    public function panel()
    {
        return $this->hasOne(Panel::class, 'nodeId', 'id');
    }

    public function bookmark()
    {
        return $this->hasOne(Bookmark::class, 'nodeId', 'id');
    }

    public function section()
    {
        return $this->hasOne(Section::class, 'nodeId', 'id');
    }

    public function grid()
    {
        return $this->hasOne(Grid::class, 'nodeId', 'id');
    }

    public function row()
    {
        return $this->hasOne(Row::class, 'nodeId', 'id');
    }

    public function cell()
    {
        return $this->hasOne(Cell::class, 'nodeId', 'id');
    }

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'userId');
    }
}