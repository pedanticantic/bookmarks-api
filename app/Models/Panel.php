<?php

namespace App\Models;

class Panel extends AbstractElement
{
    const NODE_TYPE = 'Panel';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title'];
}
