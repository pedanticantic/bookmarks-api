<?php

namespace App\Models;

class Cell extends AbstractElement
{
    const NODE_TYPE = 'Cell';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
}
