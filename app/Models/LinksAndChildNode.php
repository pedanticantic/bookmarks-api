<?php

namespace App\Models;

/**
 * @property int id
 * @property int userId
 * @property string nodeType
 * @property int $parentNodeId
 * @property int $sequenceNumber
 * @property GroupsNode[] $nodeGroups
 */
class LinksAndChildNode extends AbstractNodeType
{
    // Always include its child nodes.
    protected $with = [
        'childNodes',
        'page',
        'panel',
        'bookmark',
        'section',
        'grid',
        'row',
        'cell',
        'nodeGroups'
    ];

    public function isValidChild(LinksAndChildNode $prospectiveChild)
    {
        // Check whether this node can have the type of node that was passed in as child.
        $allowedChildrenByParent = [
            Page::NODE_TYPE => [Panel::NODE_TYPE],
            Panel::NODE_TYPE => [Section::NODE_TYPE, Bookmark::NODE_TYPE],
            Section::NODE_TYPE => [Grid::NODE_TYPE, Bookmark::NODE_TYPE],
            Grid::NODE_TYPE => [Row::NODE_TYPE],
            Row::NODE_TYPE => [Cell::NODE_TYPE],
            Cell::NODE_TYPE => [Bookmark::NODE_TYPE],
            Bookmark::NODE_TYPE => [],
        ];

        return isset($allowedChildrenByParent[ucfirst($this->nodeType)]) &&
            in_array(ucfirst($prospectiveChild->nodeType), $allowedChildrenByParent[ucfirst($this->nodeType)]);
    }

    public function childNodes()
    {
        return
            $this
                ->hasMany(LinksAndChildNode::class, 'parentNodeId', 'id')
                ->orderBy('sequenceNumber');
    }

    // @TODO: We need to make it so that by default, it limits the parent to "our" parent. Check if this even makes sense!
    // @TODO: Look at everything that calls this and make sure it works.
    public function parentNodes()
    {
        return
            $this
                ->hasMany(LinksAndChildNode::class, 'id', 'parentNodeId');
    }

    public function nodeGroups()
    {
        // @TODO: The groups should be filtered by which ones the user has access to.
        return $this->hasMany(GroupsNode::class, 'nodeId', 'id');
    }
}
