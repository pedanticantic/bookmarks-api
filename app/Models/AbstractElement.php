<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractElement extends Model
{
    public function node()
    {
        // All the elements have the same relationship with a node.
        return $this->hasOne(LinksAndChildNode::class, 'id', 'nodeId');
    }
}