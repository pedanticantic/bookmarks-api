<?php

namespace App\Models;

/**
 * @property int id
 * @property int labelId
 * @property int bookmarkId
 */
class BookmarksLabel extends AbstractElement
{
    public function label()
    {
        return $this->hasOne(Label::class, 'id', 'labelId');
    }

}
