<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $password
 * @property string $familyName
 * @property string $email
 */
class User extends Model
{
    protected $hidden = ['password', 'remember_token'];
}