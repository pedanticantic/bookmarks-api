<?php

namespace App\Models;

class Section extends AbstractElement
{
    const NODE_TYPE = 'Section';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['heading'];
}
