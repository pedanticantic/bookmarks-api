<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $groupId
 * @property string $groupName
 * @property int $createdByUserId
 * @property string $role
 * @property string $status
 * @property int $userId
 * @property string $userGivenName
 * @property string $userFamilyName
 */
class GroupMemberRole extends Model
{
    /**
     * The table (view) associated with the model.
     *
     * @var string
     */
    protected $table = 'groups_members_roles';
}