<?php

namespace App\Models;

use App\Services\Savers\LabelSaver;

/**
 * This class exists just so we can delete nodes. When querying them, etc, use LinksAndChildNode.
 * @property int $id
 * @property int $userId
 * @property string $nodeType
 * @property GroupsNode groupNodes
 */
class Node extends AbstractNodeType
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['userId', 'nodeType'];

    protected function getAssociatedRecord()
    {
        switch (ucwords($this->nodeType)) {
            case Page::NODE_TYPE:
                return $this->page;
                break;
            case Section::NODE_TYPE:
                return $this->section;
                break;
            case Panel::NODE_TYPE:
                return $this->panel;
                break;
            case Bookmark::NODE_TYPE:
                return $bookmark = $this->bookmark;
                break;
            case Grid::NODE_TYPE:
                return $this->grid;
                break;
            case Row::NODE_TYPE:
                return $this->row;
                break;
            case Cell::NODE_TYPE:
                return $this->cell;
                break;
            default:
                throw new \RuntimeException('Unknown node type "'.$this->nodeType.'" when deleting a node');
                break;
        }
    }

    public function deleteAssociatedRecord()
    {
        // Delete the associated data record, handling any special cases.
        $associatedRecord = $this->getAssociatedRecord();
        if (ucwords($this->nodeType) == Bookmark::NODE_TYPE) {
            // With bookmarks, we have to be aware of any labels on them, and delete them properly.
            // Eg if we remove a label from the bookmark, and the label isn't referenced by
            // anything else, we delete the label.
            /** @var Bookmark $associatedRecord */
            LabelSaver::updateBookmarkLabels($associatedRecord, []);
        }
        $associatedRecord->delete();
    }

    public function groupNodes()
    {
        return $this->hasMany(GroupsNode::class, 'nodeId', 'id');
    }
}