<?php

namespace App\Services;

use App\Models\LinksAndChildNode;
use App\Models\NodeParentToChild;
use App\Models\ValueObjects\ManageNodeInfo;
use Illuminate\Support\Facades\DB;

class NodeMover
{
    public static function move(ManageNodeInfo $moveNodeInfo)
    {
        $nodeToMove = $moveNodeInfo->getNodeToManage();
        $newParentNode = $moveNodeInfo->getNewParentNode();
        $newChildIndex = $moveNodeInfo->getNewChildIndex();

        // Okay, let's start moving things!
        // We have to be very careful what order we do things in, especially when moving within the same parent.
        // We need to:
        //  move the later children under the source parent up 1 (there will be a temporary duplicate with our node, of course).
        //  move the children under the destination node, from the new position onwards, down 1.
        //  update the parent and sequence number of the moving node.
        // And do it all in a transaction!
        DB::beginTransaction();
        // Shift up children under original parent.
        NodeParentToChild::where('parentNodeId', $nodeToMove->parentNodeId)
            ->where('sequenceNumber', '>', $nodeToMove->sequenceNumber)
            ->update(['sequenceNumber' => DB::raw('`sequenceNumber` - 1')]);
        // Shift down children in destination parent.
        NodeParentToChild::where('parentNodeId', $newParentNode->id)
            ->where('sequenceNumber', '>=', $newChildIndex)
            ->update(['sequenceNumber' => DB::raw('`sequenceNumber` + 1')]);
        // Update the parent & sequence number of the moving node.
        NodeParentToChild::where('childNodeId', $nodeToMove->id)
            ->where('parentNodeId', $nodeToMove->parentNodeId)
            ->update(['parentNodeId' => $newParentNode->id, 'sequenceNumber' => $newChildIndex]);
        DB::commit();

        // Now we need to return any nodes whose immediate children have been changed.
        // It/they can be simply be returned as an array.
        // We need to reload all the data, of course, as it was all changed without the models' knowledge.
        // It doesn't matter which parent link record we pick in each case - its children will be the same. I think.
        $result = [];
        if ($nodeToMove->parentNodeId != $newParentNode->id) {
            array_push($result, LinksAndChildNode::where('id', $nodeToMove->parentNodeId)->first());
        }
        array_push($result, LinksAndChildNode::where('id', $newParentNode->id)->first());

        return $result;
    }
}