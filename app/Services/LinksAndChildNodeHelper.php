<?php

namespace App\Services;

use App\Models\LinksAndChildNode;

class LinksAndChildNodeHelper
{
    public static function getForNodeAndParent(int $nodeId, int $parentNodeId = null): LinksAndChildNode
    {
        /**
         * @var LinksAndChildNode $result
         */
        $result = LinksAndChildNode::where('id', $nodeId);
        if ($parentNodeId) {
            $result->where('parentNodeId', $parentNodeId);
        } else {
            $result->whereNull('parentNodeId');
        }

        return $result->first();
    }
}