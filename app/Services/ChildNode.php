<?php

namespace App\Services;

use App\Models\LinksAndChildNode;
use App\Models\Node;
use App\Models\NodeParentToChild;
use Illuminate\Support\Facades\Auth;

class ChildNode
{
    public static function create(string $nodeType, int $parentNodeId = null, int $sequenceNumber = 1): LinksAndChildNode
    {
        // Create the child node, then create the link between it and its parent.
        $childNode = new Node([
            'userId' => Auth::id(),
            'nodeType' => $nodeType,
        ]);
        $childNode->save();
        $linkRecord = new NodeParentToChild([
            'userId' => Auth::id(),
            'parentNodeId' => $parentNodeId,
            'childNodeId' => $childNode->id,
            'sequenceNumber' => $sequenceNumber
        ]);
        $linkRecord->save();

        // Reload the two records we created above as a single entity, and return it.
        $linkAndNode = LinksAndChildNode::where('id', $childNode->id)->first();

        return $linkAndNode;
    }
}