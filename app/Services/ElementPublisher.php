<?php

namespace App\Services;


use App\Models\LinksAndChildNode;
use App\Models\Page;
use Illuminate\Support\Facades\Auth;

class ElementPublisher
{
    /**
     * @var string
     */
    private $elementType;
    /**
     * @var int
     */
    private $elementId;

    public function __construct(string $elementType, int $elementId)
    {
        $this->elementType = $elementType;
        $this->elementId = $elementId;
    }

    public function checkCanPublish()
    {
        assert(
            in_array($this->elementType, [Page::NODE_TYPE]),
            sprintf('Elements of type "%s" cannot be published', $this->elementType)
        );
        // Check the user owns the element.
        $allowed = $this->getPageNode()->count();
        assert($allowed == 1, 'You do not have access to that page');
    }

    public function alreadyPublished()
    {
        $published = $this->getPageNode()
            ->first()
            ->page
            ->token;

        return !empty($published);
    }

    public function publish()
    {
        $page = $this->getPageNode()
            ->first()
            ->page;
        // Keep generating tokens until we find one that doesn't exist in any page (usually the first one we try).
        do {
            $token = substr(md5($page->name.time().rand(0, 1000)), 0, 16);
            $exists = Page::where('token', $token)->count();
        } while ($exists);
        // Then save that token back to our page.
        $page->token = $token;
        $page->save();
    }

    public function unpublish()
    {
        // Find the page.
        $page = $this->getPageNode()
            ->first()
            ->page;
        // Clear the token in the page.
        $page->token = null;
        $page->save();
    }

    private function getPageNode()
    {
        $node = LinksAndChildNode::with('page')
            ->where('userId', Auth::id())
            ->where('nodeType', Page::NODE_TYPE)
            ->whereHas('page', function ($query) {
                $query->where('pages.id', $this->elementId);
            });

        return $node;
    }
}