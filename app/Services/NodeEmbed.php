<?php

namespace App\Services;

use App\Models\LinksAndChildNode;
use App\Models\NodeParentToChild;
use App\Models\ValueObjects\ManageNodeInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NodeEmbed
{
    public static function embed(ManageNodeInfo $embedInfo)
    {
        $nodeToEmbed = $embedInfo->getNodeToManage();
        $newParentNode = $embedInfo->getNewParentNode();
        $newChildIndex = $embedInfo->getNewChildIndex();

        // We need to make a gap in the children of the destination parent node.
        // Then create a link from the parent to our node, with the appropriate sequence number.
        DB::beginTransaction();
        // Shift down children in destination parent.
        NodeParentToChild::where('parentNodeId', $newParentNode->id)
            ->where('sequenceNumber', '>=', $newChildIndex)
            ->update(['sequenceNumber' => DB::raw('`sequenceNumber` + 1')]);
        // Create a link between the parent and our node.
        $newLink = new NodeParentToChild();
        $newLink->userId = Auth::id();
        $newLink->parentNodeId = $newParentNode->id;
        $newLink->childNodeId = $nodeToEmbed->id;
        $newLink->sequenceNumber = $newChildIndex;
        $newLink->save();
        DB::commit();

        // Now we need to return the parent node, as its children have all changed.
        // It doesn't matter which one we pick - its children will be the same. I think.
        return LinksAndChildNode::where('id', $newParentNode->id)->first();
    }
}