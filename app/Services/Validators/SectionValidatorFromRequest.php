<?php

namespace App\Services\Validators;

use App\Models\LinksAndChildNode;
use App\Models\Panel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SectionValidatorFromRequest
{
    public static function validateNew(int $parentPanelId, Request $request)
    {
        // There is no validation, as heading is optional.

        // Check that the parent panel exists and is accessible by this user.
        $panel = LinksAndChildNode::where('userId', Auth::id())
            ->where('nodeType', Panel::NODE_TYPE)
            ->join('panels', 'links_and_child_nodes.id', 'panels.nodeId')
            ->where('panels.id', $parentPanelId)
            ->count();
        if (!$panel) {
            throw new \InvalidArgumentException(sprintf('Panel with id "%d" does not exist', $parentPanelId));
        }
    }

    /**
     * Currently, the validation doesn't depend on which section is being edited.
     * @param Request $request
     */
    public static function validateExisting(Request $request/*, int $sectionId*/)
    {
        // There isn't any!
    }
}