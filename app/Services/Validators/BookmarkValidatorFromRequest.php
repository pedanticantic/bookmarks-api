<?php

namespace App\Services\Validators;

use App\Models\Cell;
use App\Models\LinksAndChildNode;
use App\Models\Panel;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookmarkValidatorFromRequest
{
    public static function validateNew(string $parentType, int $parentObjectId, Request $request)
    {
        // Check that a label and url are passed in. We allow both to be duplicated in the parent object.
        $label = $request->input('label');
        if (!$request->has('label') || empty($label)) {
            throw new \InvalidArgumentException('Label is mandatory');
        }
        $url = $request->input('url');
        if (!$request->has('url') || empty($url)) {
            throw new \InvalidArgumentException('URL is mandatory');
        }

        // Check that the parent object exists and is accessible by this user.
        switch ($parentType) {
            case Panel::NODE_TYPE:
                $parentObject = LinksAndChildNode::where('userId', Auth::id())
                    ->where('nodeType', Panel::NODE_TYPE)
                    ->join('panels', 'links_and_child_nodes.id', 'panels.nodeId')
                    ->where('panels.id', $parentObjectId)
                    ->count();
                break;
            case Section::NODE_TYPE:
                $parentObject = LinksAndChildNode::where('userId', Auth::id())
                    ->where('nodeType', Section::NODE_TYPE)
                    ->join('sections', 'links_and_child_nodes.id', 'sections.nodeId')
                    ->where('sections.id', $parentObjectId)
                    ->count();
                break;
            case Cell::NODE_TYPE:
                $parentObject = LinksAndChildNode::where('userId', Auth::id())
                    ->where('nodeType', Cell::NODE_TYPE)
                    ->join('cells', 'links_and_child_nodes.id', 'cells.nodeId')
                    ->where('cells.id', $parentObjectId)
                    ->count();
                // For a cell, you can only have 1 bookmark, so make sure the cell node has no children.
                $bookmarkCount = Cell::find($parentObjectId)
                    ->join('nodes', 'nodeId', 'cells.id')
                    ->join('node_parent_to_child', 'nodes.id', 'node_parent_to_child.parentNodeId')
                    ->count();
                if ($bookmarkCount > 0) {
                    throw new \InvalidArgumentException(
                        'A cell cannot contain more than 1 bookmark'
                    );
                }
                break;
            default:
                throw new \InvalidArgumentException(
                    'Unable to create a bookmark under an object of type: '.$parentType
                );
                break;
        }
        if (!$parentObject) {
            throw new \InvalidArgumentException(sprintf('Parent object with id "%d" does not exist', $parentObjectId));
        }
    }

    /**
     * Currently, the validation doesn't depend on which bookmark is being edited.
     * @param Request $request
     */
    public static function validateExisting(Request $request/*, int $bookmarkId*/)
    {
        // The label and URL must both be populated.
        if (!$request->has('label') || empty($request->input('label'))) {
            throw new \InvalidArgumentException('Bookmark label is mandatory');
        }
        if (!$request->has('url') || empty($request->input('url'))) {
            throw new \InvalidArgumentException('Bookmark URL is mandatory');
        }
    }
}