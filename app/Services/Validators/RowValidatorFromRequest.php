<?php

namespace App\Services\Validators;

use App\Models\Grid;
use App\Models\LinksAndChildNode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RowValidatorFromRequest
{
    public static function validateNew(int $parentGridId, Request $request)
    {
        // There is no validation, as there are no fields.

        // Check that the parent grid exists and is accessible by this user.
        $grid = LinksAndChildNode::where('userId', Auth::id())
            ->where('nodeType', Grid::NODE_TYPE)
            ->join('grids', 'links_and_child_nodes.id', 'grids.nodeId')
            ->where('grids.id', $parentGridId)
            ->count();
        if (!$grid) {
            throw new \InvalidArgumentException(sprintf('Grid with id "%d" does not exist', $parentGridId));
        }
    }

    /**
     * Currently, the validation doesn't depend on which grid is being edited.
     * @param Request $request
     */
    public static function validateExisting(Request $request/*, int $sectionId*/)
    {
        // There isn't any!
    }
}