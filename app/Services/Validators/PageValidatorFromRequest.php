<?php

namespace App\Services\Validators;

use App\Models\LinksAndChildNode;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageValidatorFromRequest
{
    public static function validateNew(Request $request)
    {
        // Check that a name is passed in, and that it doesn't already exist.
        $name = $request->input('name');
        if (!$request->has('name') || empty($name)) {
            throw new \InvalidArgumentException('Name must be supplied for a new page');
        }
        $existing = LinksAndChildNode::where('userId', Auth::id())
            ->where('nodeType', Page::NODE_TYPE)
            ->join('pages', 'links_and_child_nodes.id', 'pages.nodeId')
            ->where('name', $name)
            ->count();
        if ($existing) {
            throw new \InvalidArgumentException(sprintf('Page called "%s" already exists', $name));
        }
    }

    public static function validateExisting(Request $request, int $id)
    {
        // If they pass in a name, make sure there are no other pages with the same name.
        if ($request->has('name')) {
            if (empty($request->input('name'))) {
                throw new \InvalidArgumentException('Page name is mandatory');
            }
            $existingPage = LinksAndChildNode::where('userId', Auth::id())
                ->where('nodeType', Page::NODE_TYPE)
                ->join('pages', 'links_and_child_nodes.id', 'pages.nodeId')
                ->where('pages.name', $request->input('name'))
                ->where('pages.id', '<>', $id)
                ->count();
            if ($existingPage) {
                throw new \InvalidArgumentException('A page already exists with this name');
            }
        }
    }
}
