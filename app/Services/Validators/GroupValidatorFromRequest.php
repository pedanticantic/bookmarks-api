<?php

namespace App\Services\Validators;

use App\Models\Group;
use App\Models\GroupMember;
use App\Models\GroupMemberRole;
use App\Models\GroupsNode;
use App\Models\NodeParentToChild;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupValidatorFromRequest
{
    public static function validateNew(Request $request)
    {
        // Check that a name is passed in, and that it doesn't already exist.
        $name = $request->input('name');
        if (!$request->has('name') || empty($name)) {
            throw new \InvalidArgumentException('Name must be supplied for a new group');
        }
        $existing = Group::where('name', $name)
            ->count();
        if ($existing) {
            throw new \InvalidArgumentException(sprintf('Group called "%s" already exists', $name));
        }
    }

    public static function validateExisting(Request $request, int $id)
    {
        // If they pass in a name, make sure there are no other groups with the same name.
        if ($request->has('name')) {
            if (empty($request->input('name'))) {
                throw new \InvalidArgumentException('Group name is mandatory');
            }
            $existingGroup = Group::where('name', $request->input('name'))
                ->where('id', '<>', $id)
                ->count();
            if ($existingGroup) {
                throw new \InvalidArgumentException('A group already exists with this name');
            }
        }
    }

    public static function isInGroup(int $groupId, bool $restrictToAdmin = false, bool $restrictToMember = false)
    {
        $groupMemberRole = GroupMemberRole::where('groupId', $groupId)
            ->where('userId', Auth::id())
            ->whereIn('status', [GroupMember::STATUS_INVITED, GroupMember::STATUS_ACCEPTED]);
        if ($restrictToAdmin) {
            $groupMemberRole = $groupMemberRole->where('role', GroupMember::ROLE_ADMIN);
        }
        if ($restrictToMember) {
            $groupMemberRole = $groupMemberRole->where('role', GroupMember::ROLE_MEMBER);
        }

        return $groupMemberRole->count() > 0;
    }

    public static function validateRemoveMember(int $groupId, int $userId, bool $checkSharedNodes = true)
    {
        if ($checkSharedNodes) {
            // The member being removed must not be sharing any node with this group.
            $sharedNodes = GroupsNode::where('groupId', $groupId)
                ->whereHas('node', function ($query) use ($userId) {
                    $query->where('userId', $userId);
                })
                ->count();
            if ($sharedNodes) {
                throw new \InvalidArgumentException('Cannot remove because the user is sharing nodes with the group');
            }
        }

        // The member being removed must not have any nodes in their pages that they will no longer be able to "see".
        // Get all the nodes that the user has embedded in their pages.
        $embeddedNodes = NodeParentToChild::with(['parentNode', 'childNode', 'childNode.groupNodes'])
            ->whereHas('parentNode', function ($query) use($userId) {
                $query->where('userId', $userId);
            })
            ->whereHas('childNode', function ($query) use($userId) {
                $query->where('userId', '<>', $userId);
            })
            ->get();
        if (count($embeddedNodes)) {
            // There is at least one - get all the other groups that the user is in.
            $remainingGroups = GroupMember::where('userId', $userId)
                ->where('groupId', '<>', $groupId)
                ->where('status', GroupMember::STATUS_ACCEPTED)
                ->get();
            $groupIds = [];
            /** @var GroupMember $remainingGroup */
            foreach ($remainingGroups as $remainingGroup) {
                $groupIds[] = $remainingGroup->groupId;
            }
            // Now, loop through all their embedded nodes and if any node is not in any of the remaining
            // groups that the user is in, we need to fail the validation.
            /** @var NodeParentToChild $embeddedNode */
            foreach ($embeddedNodes as $embeddedNode) {
                $isInRemainingGroup = false;
                /** @var GroupsNode $groupNode */
                foreach ($embeddedNode->childNode->groupNodes as $groupNode) {
                    $isInRemainingGroup = $isInRemainingGroup || in_array($groupNode->groupId, $groupIds);
                }
                if (!$isInRemainingGroup) {
                    throw new \InvalidArgumentException('Cannot remove user from group - there are embedded nodes that will no longer be shared with the user');
                }
            }
        }
    }

    public static function validateDeleteGroup(int $groupId)
    {
        // Make sure the user has admin privileges on the group.
        if (!GroupValidatorFromRequest::isInGroup($groupId, true)) {
            throw new \InvalidArgumentException('You do not have privileges to delete that group');
        }

        // For each node that's been shared with the group, check that it would be safe to remove it.
        $groupMembers = GroupsNode::where('groupId', $groupId)->get();
        /** @var GroupsNode $groupMember */
        foreach ($groupMembers as $groupMember) {
            GroupNodeValidator::validateDeletion($groupId, $groupMember->nodeId);
        }

        // For each member in the group, check that it would be safe to remove them.
        $groupMembers = GroupMember::where('groupId', $groupId)->get();
        /** @var GroupMember $groupMember */
        foreach ($groupMembers as $groupMember) {
            GroupValidatorFromRequest::validateRemoveMember($groupId, $groupMember->userId, false);
        }
    }
}



