<?php

namespace App\Services\Validators;

use App\Models\GroupMember;
use App\Models\GroupsNode;
use App\Models\Node;
use App\Models\NodeParentToChild;
use Illuminate\Support\Facades\Auth;

class GroupNodeValidator
{
    /**
     * @param int $groupId
     * @param int $nodeId
     *
     * @return GroupsNode|null
     */
    public static function validateAndGetLink(int $groupId, int $nodeId)
    {
        // Check the group exists and that the user is in it.
        if (!GroupValidatorFromRequest::isInGroup($groupId)) {
            throw new \InvalidArgumentException('Group does not exist');
        }
        // Check that the node exists and that they own it.
        $node = Node::find($nodeId)->where('userId', Auth::id());
        if (!$node) {
            throw new \InvalidArgumentException('Node does not exist');
        }

        // Load any link record between the group and the node, and return it.
        return GroupsNode::where('groupId', $groupId)->where('nodeId', $nodeId)->first();
    }

    public static function validateDeletion($groupId, $nodeId)
    {
        // Check that after the node is deleted, that any instances of it being embedded will still be valid.
        // Ie that the embedding user can still "see" that node through another group.
        $embeddedBy = NodeParentToChild::with(['parentNode', 'parentNode.owner'])
            ->where('childNodeId', $nodeId)
            ->whereHas('parentNode', function ($query) {
                $query->where('userId', '<>', Auth::id());
            })
            ->get();
        $embeddingUsers = [];
        /** @var NodeParentToChild $embeddedByInstance */
        foreach ($embeddedBy as $embeddedByInstance) {
            $embeddingUsers[] = $embeddedByInstance->parentNode->owner->id;
        }
        $embeddingUsers = array_unique($embeddingUsers);
        if (count($embeddingUsers)) {
            $otherGroups = GroupsNode::where('nodeId', $nodeId)
                ->where('groupId', '<>', $groupId)
                ->select('groupId')
                ->groupBy('groupId')
                ->get();
            $otherGroupIds = [];
            /** @var GroupsNode $otherGroup */
            foreach($otherGroups as $otherGroup) {
                $otherGroupIds[] = $otherGroup->groupId;
            }
            foreach ($embeddingUsers as $embeddingUser) {
                $inOtherGroup = GroupMember::where('userId', $embeddingUser)
                    ->where('status', GroupMember::STATUS_ACCEPTED)
                    ->whereIn('groupId', $otherGroupIds)
                    ->count();
                if ($inOtherGroup == 0) {
                    throw new \InvalidArgumentException('Cannot remove node from group - other people have embedded it in their pages');
                }
            }
        }
    }
}