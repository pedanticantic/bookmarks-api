<?php

namespace App\Services\Validators;

use App\Models\LinksAndChildNode;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GridValidatorFromRequest
{
    public static function validateNew(int $parentSectionId, Request $request)
    {
        // There is no validation, as there are no fields.

        // Check that the parent section exists and is accessible by this user.
        $section = LinksAndChildNode::where('userId', Auth::id())
            ->where('nodeType', Section::NODE_TYPE)
            ->join('sections', 'links_and_child_nodes.id', 'sections.nodeId')
            ->where('sections.id', $parentSectionId)
            ->count();
        if (!$section) {
            throw new \InvalidArgumentException(sprintf('Section with id "%d" does not exist', $parentSectionId));
        }
    }

    /**
     * Currently, the validation doesn't depend on which section is being edited.
     * @param Request $request
     */
    public static function validateExisting(Request $request/*, int $sectionId*/)
    {
        // There isn't any!
    }
}