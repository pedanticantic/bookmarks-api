<?php

namespace App\Services\Validators;

use App\Models\LinksAndChildNode;
use App\Models\ValueObjects\ManageNodeInfo;
use App\Services\LinksAndChildNodeHelper;
use App\Services\NodeSharing;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

// @TODO: I'm using request() temporarily - should pass the request object in, really.
class NodeMoveValidator
{
    public static function validateMove(int $nodeId): ManageNodeInfo
    {
        // Check that the right parameters have been passed - 2 node ids and an index.
        if (!(request()->has('currentParentNodeId') && request()->has('newParentNodeId') && request()->has('newChildIndex'))) {
            throw new \InvalidArgumentException('Not all parameters are present');
        }

        // Check that the two nodes exist and are accessible to the caller.
        /**
         * @var LinksAndChildNode $newParentNode
         */
        $nodeToMove = LinksAndChildNodeHelper::getForNodeAndParent($nodeId, request()->get('currentParentNodeId'));
        if (!$nodeToMove || ($nodeToMove->userId != Auth::id() && !NodeSharing::nodeIsSharedWithUser($nodeId, Auth::id()))) {
            throw new \RuntimeException('Node being moved does not exist');
        }
        $newParentNode = LinksAndChildNode::where('id', request()->get('newParentNodeId'))->first();
        if (!$newParentNode || $newParentNode->userId != Auth::id()) {
            throw new \RuntimeException('Parent node does not exist');
        }
        if (!$newParentNode->isValidChild($nodeToMove)) {
            throw new \RuntimeException('Cannot move that node to that parent');
        }

        // Check that the new child index is sensible.
        // Remember the node sequence number uses a 1-based index.
        $newChildIndex = request()->get('newChildIndex');
        $maxChildIndex = $newParentNode->childNodes()->max('sequenceNumber');
        $maxChildIndex = $maxChildIndex ? $maxChildIndex : 1; // New parent might not have any children at the moment.
        if ($nodeToMove->parentNodeId == $newParentNode->id) {
            $maxChildIndex--; // If we're moving within the same parent, you can't go beyond the end of the children.
        }
        if ($newChildIndex < 0 || $newChildIndex > (1 + $maxChildIndex)) {
            throw new \RuntimeException('New child index is out of range');
        }

        // We now need to return a value object containing those 3 things.
        return new ManageNodeInfo($nodeToMove, $newParentNode, $newChildIndex);
    }
}
