<?php

namespace App\Services\Validators;

use App\Models\LinksAndChildNode;
use App\Models\Node;
use App\Models\ValueObjects\ManageNodeInfo;
use App\Services\NodeSharing;
use App\Services\NodeTreeTraverser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

// @TODO: I'm using request() temporarily - should pass the request object in, really.
class NodeEmbedValidator
{
    public static function validateEmbed(int $nodeId)
    {
        // Check that the right parameters have been passed - parent node id and the new index.
        if (!(request()->has('newParentNodeId') && request()->has('newChildIndex'))) {
            throw new \InvalidArgumentException('Not all parameters are present');
        }

        // Check that the node being embedded is accessible to the user (ie it's shared with a group that the user is in).
        if (!NodeSharing::nodeIsSharedWithUser($nodeId, Auth::id())) {
            throw new \RuntimeException('The node you are embedding has not been shared with you');
        }
        $embedNode = LinksAndChildNode::where('id', $nodeId)->first();

        // Check parent node exists and is owned by the user.
        /** @var LinksAndChildNode $parentNode */
        $parentNode = LinksAndChildNode::where('id', request()->get('newParentNodeId'))->first();
        if (!$parentNode || $parentNode->userId != Auth::id()) {
            throw new \RuntimeException('You do not own the node that the node is being embedded into');
        }
        if (!$parentNode->isValidChild($embedNode)) {
            throw new \RuntimeException('Cannot embed that node under that parent');
        }

        // Check that the new index is valid.
        // Remember the node sequence number uses a 1-based index.
        $newChildIndex = request()->get('newChildIndex');
        $maxChildIndex = $parentNode->childNodes()->max('sequenceNumber');
        $maxChildIndex = $maxChildIndex ? $maxChildIndex : 1; // New parent might not have any children at the moment.
        if ($newChildIndex < 0 || $newChildIndex > (1 + $maxChildIndex)) {
            throw new \RuntimeException('New child index is out of range');
        }

        // Check that the node is not already in the same page as the one with the parent node.
        $nodePageIds = array_map(
            function (Node $page) {
                return $page->id;
            },
            NodeTreeTraverser::getPagesFor(Node::find($nodeId))
        );
        $parentPageIds = array_map(
            function (Node $page) {
                return $page->id;
            },
            array_filter(
                NodeTreeTraverser::getPagesFor(Node::find(request()->get('newParentNodeId'))),
                function (Node $page) {
                    return $page->userId == Auth::id();
                }
            )
        );
        if (count(array_intersect($nodePageIds, $parentPageIds))) {
            throw new \RuntimeException('You cannot embed a node into a page more than once');
        }

        return new ManageNodeInfo($embedNode, $parentNode, $newChildIndex);
    }
}
