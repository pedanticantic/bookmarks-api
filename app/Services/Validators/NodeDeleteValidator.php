<?php

namespace App\Services\Validators;

use App\Models\Bookmark;
use App\Models\Grid;
use App\Models\GroupsNode;
use App\Models\LinksAndChildNode;
use App\Models\Node;
use App\Models\Page;
use App\Models\Panel;
use App\Models\Row;
use App\Models\Section;
use App\Services\LinksAndChildNodeHelper;
use Illuminate\Support\Facades\Auth;

class NodeDeleteValidator
{
    public static function validateDelete(int $nodeId, int $parentNodeId = null)
    {
        // Load the node and check it exists and that it is in an appropriate state to delete.
        $result = LinksAndChildNodeHelper::getForNodeAndParent($nodeId, $parentNodeId);
        if (!$result) {
            throw new \RuntimeException('Node being deleted does not exist');
        }
        if ($parentNodeId) {
            if (Node::find($parentNodeId)->userId != Auth::id()) {
                throw new \RuntimeException('You do not own the parent node');
            }
        }

        // If the user is to trying to remove an embedded node from their page, it is always allowed.
        if ($result->userId == Auth::id()) {
            // User is managing their own nodes in a page.
            // If it is shared with any group, prevent deletion.
            if (count($result->nodeGroups)) {
                throw new \RuntimeException('Cannot delete a node that is shared with any groups');
            }

            // Bookmarks and grids can always be deleted.
            // Pages, panels and sections can only be deleted if they have no children.
            // grid rows can only be deleted if they have at least one sibling.
            // Everything else cannot be deleted.
            switch (ucwords($result->nodeType)) {
                case Bookmark::NODE_TYPE:
                case Grid::NODE_TYPE:
                    break;
                case Page::NODE_TYPE:
                case Panel::NODE_TYPE:
                case Section::NODE_TYPE:
                    if ($result->childNodes()->count()) {
                        throw new \InvalidArgumentException(
                            'Cannot delete a page, panel or section if it is not empty.'
                        );
                    }
                    break;
                case Row::NODE_TYPE:
                    // @TODO: We will need to do more filtering on the parent node in phase 2.
                    if (LinksAndChildNode::where('id', $result->parentNodes()->first()->id)->first()->childNodes()->count() <= 1) {
                        throw new \InvalidArgumentException('Cannot delete the only row in a grid.');
                    }
                    break;
                default:
                    throw new \InvalidArgumentException('Unknown type of node ('.$result->nodeType.')');
                    break;
            }
        }

        return $result;
    }
}