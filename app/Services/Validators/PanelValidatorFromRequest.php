<?php

namespace App\Services\Validators;

use App\Models\LinksAndChildNode;
use App\Models\Page;
use App\Models\Panel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PanelValidatorFromRequest
{
    public static function validateNew(int $parentPageId, Request $request)
    {
        // Check that the parent page exists and is accessible by this user.
        $page = LinksAndChildNode::where('userId', Auth::id())
            ->where('nodeType', Page::NODE_TYPE)
            ->join('pages', 'links_and_child_nodes.id', 'pages.nodeId')
            ->where('pages.id', $parentPageId)
            ->count();
        if (!$page) {
            throw new \InvalidArgumentException(sprintf('Page with id "%d" does not exist', $parentPageId));
        }

        // Check that a title is passed in, and that it doesn't already exist in this page.
        $title = $request->input('title');
        if (!$request->has('title') || empty($title)) {
            throw new \InvalidArgumentException('Title is mandatory');
        }
        $existing = LinksAndChildNode::where('links_and_child_nodes.userId', Auth::id())
            ->where('links_and_child_nodes.nodeType', Page::NODE_TYPE)
            ->join('pages', 'links_and_child_nodes.id', 'pages.nodeId')
            ->where('pages.id', $parentPageId)
            ->join('links_and_child_nodes AS panelNode', 'panelNode.parentNodeId', 'links_and_child_nodes.id')
            ->where('panelNode.nodeType', Panel::NODE_TYPE)
            ->join('panels', 'panelNode.id', 'panels.nodeId')
            ->where('title', $title)
            ->count();
        if ($existing) {
            throw new \InvalidArgumentException(sprintf('Panel with title "%s" already exists', $title));
        }
    }

    public static function validateExisting(Request $request, int $panelId)
    {
        // If they pass in a title, make sure there are no other panels with the same title in the page.
        if ($request->has('title')) {
            if (empty($request->input('title'))) {
                throw new \InvalidArgumentException('Panel title is mandatory');
            }
            $parentNode = LinksAndChildNode::where('userId', Auth::id())
                ->where('nodeType', Panel::NODE_TYPE)
                ->whereHas('panel', function ($query) use ($panelId) {
                    $query->where('panels.id', $panelId);
                })
                ->first();
            if (!$parentNode) {
                throw new \InvalidArgumentException('That panel does not exist');
            }
            $existingPanel = LinksAndChildNode::where('parentNodeId', $parentNode->parentNodeId)
                ->whereHas('panel', function ($query) use ($panelId, $request) {
                    $query->where('panels.title', $request->input('title'));
                    $query->where('panels.id', '<>', $panelId);
                })
                ->count();
            if ($existingPanel) {
                throw new \InvalidArgumentException('A panel already exists in this page with this title');
            }
        }
    }
}