<?php

namespace App\Services;

use App\Models\GroupMember;

class NodeSharing
{
    public static function nodeIsSharedWithUser(int $nodeId, int $userId)
    {
        $shared = GroupMember::where('userId', $userId)
            ->where('status', GroupMember::STATUS_ACCEPTED)
            ->join('groups_nodes', 'groups_members.groupId', 'groups_nodes.groupId')
            ->where('groups_nodes.nodeId', $nodeId)
            ->count();

        return $shared > 0;
    }
}