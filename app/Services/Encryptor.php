<?php

namespace App\Services;

use App\Models\User;

class Encryptor
{
    public static function getForUser(User $user, int $expiryTime)
    {
        return md5($user->id.':'.$expiryTime.':'.env('ENCRYPTOR_SALT'));
    }
}