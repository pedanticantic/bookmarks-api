<?php

namespace App\Services;

use App\Models\Cell;
use App\Models\Grid;
use App\Models\LinksAndChildNode;
use App\Models\Row;

class GridStructureEnforcer
{
    /**
     * The given (empty) grid has just been created. Make sure it has the right rows and columns.
     * @param LinksAndChildNode $gridNode
     */
    public static function afterGridCreate(LinksAndChildNode $gridNode)
    {
        // @TODO: In theory we should check that there are no rows and columns, and handle it appropriately.
        // @TODO: Eg if rows exist, just call the method that makes sure there are the right columns (cells) in all the rows.

        // There must be at least one row; currently there are zero. Create a row.
        $rowNode = ChildNode::create(Row::NODE_TYPE, $gridNode->id);
        $row = new Row();
        $row->nodeId = $rowNode->id;
        $row->save();

        // Our one row must now have at least one column (cell); currently it has zero. Create a cell.
        // There will be no bookmark in the cell.
        // @TODO: We could just call static::afterRowCreate() here. Just make sure that method handles the creation of the first row in a table.
        $cellNode = ChildNode::create(Cell::NODE_TYPE, $rowNode->id);
        $cell = new Cell();
        $cell->nodeId = $cellNode->id;
        $cell->save();
    }

    /**
     * The given (empty) row has just been created in a grid. Make sure it has the right number of
     * columns (cells) in it.
     * @param LinksAndChildNode $rowNode
     */
    public static function afterRowCreate(LinksAndChildNode $rowNode)
    {
        // Get the number of cells this row should have.
        $maxCols = static::getMaxCols($rowNode);

        // Make sure this row has "maxCols" cells.
        static::createAppropriateCells($rowNode, $maxCols);
    }

    public static function afterCellCreate(LinksAndChildNode $cellNode)
    {
        // We need to know how many columns are in the row with the most columns.
        /**
         * @var LinksAndChildNode $rowNode
         */
        $rowNode = $cellNode->parentNodes()->first();
        $maxCols = static::getMaxCols($rowNode);

        // Now loop through all the rows, and make sure they have maxCols cells.
        /**
         * @var LinksAndChildNode $gridNode
         */
        $gridNode = $rowNode->parentNodes()->first();
        foreach ($gridNode->childNodes()->get() as $rowNode) {
            static::createAppropriateCells($rowNode, $maxCols);
        }
    }

    protected static function getMaxCols(LinksAndChildNode $rowNode)
    {
        // Need to find the max number of cells across all rows in this grid.
        /**
         * @var LinksAndChildNode $gridNode
         */
        $gridNode = LinksAndChildNode::where('nodeType', Grid::NODE_TYPE)
            ->where('id', $rowNode->parentNodeId)
            ->first();
        $rows = $gridNode->childNodes()->get();
        $maxCols = -1;
        /**
         * @var LinksAndChildNode $row
         */
        foreach ($rows as $row) {
            $cellCount = $row->childNodes()->count();
            $maxCols = max($maxCols, $cellCount);
        }

        return $maxCols;
    }

    protected static function createAppropriateCells(LinksAndChildNode $rowNode, int $maxCols)
    {
        // Find how many cells it currently has. We're going to assume the sequence numbers are correct.
        // Note: node sequence numbers use a 1-based index.
        $currentNumCols = $rowNode->childNodes()->count();
        for ($colSeqNo = 1 + $currentNumCols ; $colSeqNo <= $maxCols ; $colSeqNo++) {
            $cellNode = ChildNode::create(Cell::NODE_TYPE, $rowNode->id, $colSeqNo);
            $cell = new Cell();
            $cell->nodeId = $cellNode->id;
            $cell->save();
        }
    }
}