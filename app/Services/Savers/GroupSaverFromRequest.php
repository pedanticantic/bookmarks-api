<?php

namespace App\Services\Savers;

use App\Models\Group;
use App\Models\GroupMember;
use App\Models\GroupsNode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GroupSaverFromRequest
{
    public static function saveNew(Request $request): Group
    {
        // Try to create a group.
        DB::beginTransaction();
        $data = $request->all();
        $group = new Group($data);
        $group->createdByUserId = Auth::id();
        $group->save();
        // Add this person as a member.
        static::addMember($group, Auth::id(), GroupMember::ROLE_ADMIN, GroupMember::STATUS_ACCEPTED);
        DB::commit();

        return $group;
    }

    public static function saveExisting(Request $request, int $id): Group
    {
        $group = Group::find($id);
        $group->fill($request->all());
        $group->save();

        return $group;
    }

    protected static function addMember(Group $group, int $userId, string $role, string $status = GroupMember::STATUS_INVITED)
    {
        $groupMember = new GroupMember();
        $groupMember->groupId = $group->id;
        $groupMember->userId = $userId;
        $groupMember->role = $role;
        $groupMember->status = $status;
        $groupMember->save();
    }

    public static function delete(int $groupId)
    {
        DB::beginTransaction();
        // Remove all its members.
        GroupMember::where('groupId', $groupId)->delete();
        // Remove all its nodes.
        GroupsNode::where('groupId', $groupId)->delete();
        // Delete the group.
        Group::find($groupId)->delete();
        DB::commit();
    }
}