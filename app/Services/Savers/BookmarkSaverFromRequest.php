<?php

namespace App\Services\Savers;

use App\Models\Bookmark;
use App\Models\Cell;
use App\Models\Grid;
use App\Models\LinksAndChildNode;
use App\Models\Panel;
use App\Models\Section;
use App\Services\ChildNode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BookmarkSaverFromRequest
{
    public static function saveNew(string $parentType, int $parentObjectId, Request $request)
    {
        // We want the node with the highest sequence number for the given parent object.
        switch ($parentType) {
            case Panel::NODE_TYPE:
                $nodeAndObject = LinksAndChildNode::where('userID', Auth::id())
                    ->where('nodeType', Panel::NODE_TYPE)
                    ->whereHas('panel', function ($query) use ($parentObjectId) {
                        $query->where('panels.id', $parentObjectId);
                    })
                    ->with('panel')
                    ->first();
                break;
            case Section::NODE_TYPE:
                $nodeAndObject = LinksAndChildNode::where('userID', Auth::id())
                    ->where('nodeType', Section::NODE_TYPE)
                    ->whereHas('section', function ($query) use ($parentObjectId) {
                        $query->where('sections.id', $parentObjectId);
                    })
                    ->with('section')
                    ->first();
                break;
            case Cell::NODE_TYPE:
                $nodeAndObject = LinksAndChildNode::where('userID', Auth::id())
                    ->where('nodeType', Cell::NODE_TYPE)
                    ->whereHas('cell', function ($query) use ($parentObjectId) {
                        $query->where('cells.id', $parentObjectId);
                    })
                    ->with('cell')
                    ->first();
                break;
            default:
                throw new \InvalidArgumentException(
                    'Unable to save a bookmark under an object of type: '.$parentType
                );
                break;
        }
        $nextParentSeqNo = LinksAndChildNode::whereIn('nodeType', [Bookmark::NODE_TYPE, Section::NODE_TYPE, Grid::NODE_TYPE])
                ->where('parentNodeId', $nodeAndObject->id)
                ->max('sequenceNumber') ?? 0;
        ++$nextParentSeqNo;

        // Try to create a bookmark (bookmark node plus parent node link plus bookmark).
        DB::beginTransaction();
        $data = $request->all();
        $bookmarkNode = ChildNode::create(Bookmark::NODE_TYPE, $nodeAndObject->id, $nextParentSeqNo);
        $bookmark = new Bookmark($data);
        $bookmark->nodeId = $bookmarkNode->id;
        $bookmark->save();
        LabelSaver::updateBookmarkLabels($bookmark, $request->get('labels'));
        DB::commit();

        return $bookmark;
    }

    public static function saveExisting(Request $request, int $id): Bookmark
    {
        DB::beginTransaction();
        $bookmark = Bookmark::find($id);
        $bookmark->fill($request->all());
        $bookmark->save();
        LabelSaver::updateBookmarkLabels($bookmark, $request->get('labels'));
        DB::commit();

        return $bookmark;
    }
}