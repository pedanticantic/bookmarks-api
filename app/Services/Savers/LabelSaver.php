<?php

namespace App\Services\Savers;

use App\Models\Bookmark;
use App\Models\BookmarksLabel;
use App\Models\Label;
use Illuminate\Support\Facades\Auth;

class LabelSaver
{
    public static function updateBookmarkLabels(Bookmark $bookmark, array $rawLabels)
    {
        // $rawLabels is the exact set of labels that we want on the bookmark.
        // If there's a label against the bookmark that's not in the set, we need to detach it.
        // For any label that we're attaching, if the label itself doesn't exist in the DB, then save it.
        // Loop through the passed-in labels, and process any "attaches". Maintain an array of the labels
        // that should be on the bookmark - at the end we detach any that aren't
        $keepLabels = [];
        foreach ($rawLabels as $rawLabel) {

            $label = new Label();
            if (!empty($rawLabel['id'])) {
                $label->id = $rawLabel['id'];
            }
            $label->name = $rawLabel['name'];

            if (empty($label->id)) {
                // Save the label itself to the DB.
                static::saveNew($label);
                // We know it can't possibly have been attached to this bookmark, so attach it.
                static::attachToBookmark($bookmark, $label);
            } else {
                // It's an existing label. It may or may not be attached to the bookmark. If not, attach it.
                $bookmarkLabel = BookmarksLabel::where('labelId', $label->id)
                    ->where('bookmarkId', $bookmark->id)
                    ->first();
                if (empty($bookmarkLabel)) {
                    static::attachToBookmark($bookmark, $label);
                }
            }

            $keepLabels[] = $label->name;
        }

        // Load all the labels against the bookmarks that are NOT in the set that was passed in, and detach them.
        // For any label we detach, if it's no longer attached to any bookmarks, delete the label.
        $toDelete = BookmarksLabel::where('bookmarkId', $bookmark->id)
            ->whereDoesntHave('label', function ($query) use ($keepLabels) {
                $query->whereIn('labels.name', $keepLabels);
            })
            ->get();
        /**
         * @var BookmarksLabel $bookmarkLabel
         */
        foreach ($toDelete as $bookmarkLabel) {
            static::detachLabel($bookmarkLabel);
        }
    }

    private static function saveNew(Label $label)
    {
        $label->userId = Auth::id();
        $label->save();
    }

    private static function attachToBookmark(Bookmark $bookmark, Label $label)
    {
        $bookmarkLabel = new BookmarksLabel();
        $bookmarkLabel->labelId = $label->id;
        $bookmarkLabel->bookmarkId = $bookmark->id;
        $bookmarkLabel->save();
    }

    private static function detachLabel(BookmarksLabel $bookmarkLabel)
    {
        // Delete the given link record, but also, if the label is not attached to anything else, delete
        // the label itself.
        $bookmarkLabel->delete();
        $remaining = BookmarksLabel::where('labelId', $bookmarkLabel->labelId)->count();
        if ($remaining == 0) {
            $label = Label::find($bookmarkLabel->labelId);
            $label->delete();
        }
    }
}