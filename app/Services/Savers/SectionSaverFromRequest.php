<?php

namespace App\Services\Savers;

use App\Models\Bookmark;
use App\Models\Panel;
use App\Models\Section;
use App\Models\LinksAndChildNode;
use App\Services\ChildNode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SectionSaverFromRequest
{
    public static function saveNew(int $parentPanelId, Request $request)
    {
        // We want the node with the highest sequence number for the given parent panel.
        $nodeAndPanel = LinksAndChildNode::where('userID', Auth::id())
            ->where('nodeType', Panel::NODE_TYPE)
            ->whereHas('panel', function ($query) use ($parentPanelId) {
                $query->where('panels.id', $parentPanelId);
            })
            ->with('panel')
            ->first();
        $nextPanelSeqNo = LinksAndChildNode::whereIn('nodeType', [Bookmark::NODE_TYPE, Section::NODE_TYPE])
                ->where('parentNodeId', $nodeAndPanel->id)
                ->max('sequenceNumber') ?? 0;
        ++$nextPanelSeqNo;

        // Try to create a section (section node plus parent node link plus section).
        DB::beginTransaction();
        $data = $request->all();
        $sectionNode = ChildNode::create(Section::NODE_TYPE, $nodeAndPanel->id, $nextPanelSeqNo);
        $section = new Section($data);
        $section->nodeId = $sectionNode->id;
        $section->save();
        DB::commit();

        return $section;
    }

    public static function saveExisting(Request $request, int $id): Section
    {
        $section = Section::find($id);
        $section->fill($request->all());
        $section->save();

        return $section;
    }
}