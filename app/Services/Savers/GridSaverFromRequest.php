<?php

namespace App\Services\Savers;

use App\Models\Grid;
use App\Models\LinksAndChildNode;
use App\Models\Section;
use App\Services\ChildNode;
use App\Services\GridStructureEnforcer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GridSaverFromRequest
{
    public static function saveNew(int $parentSectionId, Request $request)
    {
        // We want the node with the highest sequence number for the given parent section.
        $nodeAndSection = LinksAndChildNode::where('userID', Auth::id())
            ->where('nodeType', Section::NODE_TYPE)
            ->whereHas('section', function ($query) use ($parentSectionId) {
                $query->where('sections.id', $parentSectionId);
            })
            ->with('section')
            ->first();
        $nextSectionSeqNo = LinksAndChildNode::where('nodeType', Grid::NODE_TYPE)
            ->where('parentNodeId', $nodeAndSection->id)
            ->max('sequenceNumber') ?? 0;
        ++$nextSectionSeqNo;

        // Try to create a grid (grid node plus parent node link plus grid).
        DB::beginTransaction();
        $data = $request->all();
        $gridNode = ChildNode::create(Grid::NODE_TYPE, $nodeAndSection->id, $nextSectionSeqNo);
        $grid = new Grid($data);
        $grid->nodeId = $gridNode->id;
        $grid->save();
        // Grids are special in that there must be at least 1 row and 1 column and they must be a proper
        // rectangle, so call the method that makes sure all those things are satisfied. Note we are still
        // in the transaction.
        GridStructureEnforcer::afterGridCreate($gridNode);
        DB::commit();

        return $grid;
    }

    public static function saveExisting(Request $request, int $id): Grid
    {
        $grid = Grid::find($id);
        $grid->fill($request->all());
        $grid->save();

        return $grid;
    }
}