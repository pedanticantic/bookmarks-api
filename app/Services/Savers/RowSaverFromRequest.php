<?php

namespace App\Services\Savers;

use App\Models\Grid;
use App\Models\LinksAndChildNode;
use App\Models\Row;
use App\Services\ChildNode;
use App\Services\GridStructureEnforcer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RowSaverFromRequest
{
    public static function saveNew(int $parentGridId, Request $request)
    {
        // We want the node with the highest sequence number for the given parent grid.
        $nodeAndGrid = LinksAndChildNode::where('userID', Auth::id())
            ->where('nodeType', Grid::NODE_TYPE)
            ->whereHas('grid', function ($query) use ($parentGridId) {
                $query->where('grids.id', $parentGridId);
            })
            ->with('grid')
            ->first();
        $nextRowSeqNo = LinksAndChildNode::where('nodeType', Row::NODE_TYPE)
                ->where('parentNodeId', $nodeAndGrid->id)
                ->max('sequenceNumber') ?? 0;
        ++$nextRowSeqNo;

        // Try to create a row (row node plus parent node link plus row).
        DB::beginTransaction();
        $data = $request->all();
        $rowNode = ChildNode::create(Row::NODE_TYPE, $nodeAndGrid->id, $nextRowSeqNo);
        $row = new Row($data);
        $row->nodeId = $rowNode->id;
        $row->save();
        // We have added a row. It will have no columns. Call the method that makes sure a grid is a
        // proper rectangle, and it will add the appropriate columns.
        GridStructureEnforcer::afterRowCreate($rowNode);
        DB::commit();

        return $row;
    }
}