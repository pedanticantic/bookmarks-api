<?php

namespace App\Services\Savers;

use App\Models\Cell;
use App\Models\Grid;
use App\Models\LinksAndChildNode;
use App\Models\Row;
use App\Services\ChildNode;
use App\Services\GridStructureEnforcer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ColumnSaverFromRequest
{
    public static function saveNew(int $parentGridId, Request $request)
    {
        // We want the first row in the grid. Then we want the highest cell sequence number in that row.
        $nodeAndGrid = LinksAndChildNode::where('userID', Auth::id())
            ->where('nodeType', Grid::NODE_TYPE)
            ->whereHas('grid', function ($query) use ($parentGridId) {
                $query->where('grids.id', $parentGridId);
            })
            ->with('grid')
            ->first();
        /**
         * @var LinksAndChildNode $rowNode
         */
        $rowNode = LinksAndChildNode::where('nodeType', Row::NODE_TYPE)
            ->where('parentNodeId', $nodeAndGrid->id)
            ->orderBy('sequenceNumber')
            ->first();
        $nextCellSeqNo = LinksAndChildNode::where('nodeType', Cell::NODE_TYPE)
                ->where('parentNodeId', $rowNode->id)
                ->max('sequenceNumber') ?? 0;
        ++$nextCellSeqNo;

        // Try to create a cell (cell node plus parent node link plus cell).
        DB::beginTransaction();
        $data = $request->all();
        $cellNode = ChildNode::create(Cell::NODE_TYPE, $rowNode->id, $nextCellSeqNo);
        $cell = new Cell($data);
        $cell->nodeId = $cellNode->id;
        $cell->save();
        // We have added a cell to the first row. It will have one more cell than all the other rows.
        // Call the method that makes sure a grid is a proper rectangle, and it will add the appropriate
        // cells to the other rows.
        GridStructureEnforcer::afterCellCreate($cellNode);
        DB::commit();

        return $rowNode->parentNodes()->first()->grid()->first();
    }
}