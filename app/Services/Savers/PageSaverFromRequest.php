<?php

namespace App\Services\Savers;

use App\Models\LinksAndChildNode;
use App\Models\Page;
use App\Services\ChildNode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PageSaverFromRequest
{
    public static function saveNew(Request $request): Page
    {
        // Okay, find the existing pages for the user. Specifically, we want the highest page seq no.
        $nextPageSeqNo = LinksAndChildNode::where('userID', Auth::id())
            ->where('nodeType', Page::NODE_TYPE)
            ->whereNull('parentNodeId')
            ->max('sequenceNumber') ?? 0;
        ++$nextPageSeqNo;

        // Try to create a page (page node plus parent node link plus page).
        DB::beginTransaction();
        $data = $request->all();
        $pageNode = ChildNode::create(Page::NODE_TYPE, null, $nextPageSeqNo);
        $page = new Page($data);
        $page->nodeId = $pageNode->id;
        $page->save();
        DB::commit();

        return $page;
    }

    public static function saveExisting(Request $request, int $id): Page
    {
        $page = Page::find($id);
        $page->fill($request->all());
        $page->save();

        return $page;
    }
}