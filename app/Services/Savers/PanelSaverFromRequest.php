<?php

namespace App\Services\Savers;

use App\Models\LinksAndChildNode;
use App\Models\Page;
use App\Models\Panel;
use App\Services\ChildNode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PanelSaverFromRequest
{
    public static function saveNew(int $parentPageId, Request $request)
    {
        // We want the title with the highest sequence number for the given parent page.
        $nodeAndPage = LinksAndChildNode::where('userID', Auth::id())
            ->where('nodeType', Page::NODE_TYPE)
            ->whereHas('page', function ($query) use ($parentPageId) {
                $query->where('pages.id', $parentPageId);
            })
            ->with('page');
        $nextPageSeqNo = LinksAndChildNode::where('nodeType', Panel::NODE_TYPE)
            ->where('parentNodeId', $nodeAndPage->first()->id)
            ->max('sequenceNumber') ?? 0;
        ++$nextPageSeqNo;

        // Try to create a panel (panel node plus parent node link plus page).
        DB::beginTransaction();
        $data = $request->all();
        $panelNode = ChildNode::create(Panel::NODE_TYPE, $nodeAndPage->first()->id, $nextPageSeqNo);
        $panel = new Panel($data);
        $panel->nodeId = $panelNode->id;
        $panel->save();
        DB::commit();

        return $panel;
    }

    public static function saveExisting(Request $request, int $id): Panel
    {
        $panel = Panel::find($id);
        $panel->fill($request->all());
        $panel->save();

        return $panel;
    }
}