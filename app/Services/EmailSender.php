<?php

namespace App\Services;

use App\Mail\AccountDeleted;
use App\Mail\ChangedPassword;
use App\Mail\ContactUs;
use App\Mail\EmailChanged;
use App\Mail\MemberInvited;
use App\Mail\ProfileChanged;
use App\Mail\ResetPassword;
use App\Mail\Welcome;
use App\Models\Group;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class EmailSender
{
    public static function sendMemberInvite(User $user, Group $group, string $managementUrl)
    {
        Mail::to($user->email)->send(new MemberInvited($user, $group, $managementUrl));
    }

    public static function sendForgottenPasswordLink(
        User $user,
        string $resetPasswordUrl,
        int $expirySeconds = null
    ) {
        Mail::to($user->email)->send(new ResetPassword($user, $resetPasswordUrl, $expirySeconds));
    }

    public static function newEmailOldAddress(User $user)
    {
        Mail::to($user->email)->send(new EmailChanged($user, false));
    }

    public static function newEmailNewAddress(User $user)
    {
        Mail::to($user->email)->send(new EmailChanged($user, true));
    }

    public static function passwordChanged(User $user)
    {
        Mail::to($user->email)->send(new ChangedPassword($user));
    }

    public static function profileChanged(User $user)
    {
        Mail::to($user->email)->send(new ProfileChanged($user));
    }

    public static function welcomeNewUser(User $user)
    {
        Mail::to($user->email)->send(new Welcome($user));
    }

    public static function accountDeleted(User $user)
    {
        Mail::to($user->email)->send(new AccountDeleted($user));
    }

    public static function sendContactForm($fromAddress, $subject, $details)
    {
        $toAddress = env('CONTACT_TO_ADDRESS', 'pedanticantic+contact-us@gmail.com');
        Mail::to($toAddress)->send(new ContactUs($fromAddress, $subject, $details));
    }
}
