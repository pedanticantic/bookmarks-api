<?php

namespace App\Services;

use App\Models\LinksAndChildNode;
use App\Models\Node;
use App\Models\NodeParentToChild;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NodeDeleter
{
    public static function delete(LinksAndChildNode $node)
    {
        // Load the node's parent node for later.
        /**
         * @var Node $parentNode
         */
        $parentNode = Node::find($node->parentNodeId);

        // Delete the node and everything associated with it.
        DB::beginTransaction();
        if ($node->userId == Auth::id()) {
            // Delete this node and its children.
            static::deleteNode($node);
        } else {
            static::unembedNode($node);
        }
        // Decrement the sequence number(s) of the later sibling node(s), unless we just deleted a page.
        if ($parentNode) {
            $children = NodeParentToChild::where('parentNodeId', $parentNode->id);
        } else {
            $children = NodeParentToChild::whereNull('parentNodeId')
                ->where('userId', Auth::id());
        }
        $children->where('sequenceNumber', '>', $node->sequenceNumber)
            ->decrement('sequenceNumber');
        DB::commit();

        // Return the parent node (unless we just deleted a page).
        /**
         * @var LinksAndChildNode|null $result
         */
        $result = null;
        if ($parentNode) {
            // It doesn't matter which one we pick - its children will be the same. I think.
            $result = LinksAndChildNode::where('id', $parentNode->id)->first();
            if (!$result) {
                throw new \RuntimeException('Could not find parent node');
            }
        }

        return $result;
    }

    /**
     * Recursive method that deletes a node and all its children. I didn't want to handle this in an
     * event handler because I only want the node deleter to be able to do this. Ie I don't want some
     * other random class/method to be able to delete a whole hierarchy of nodes by mistake.
     * Naturally, it calls itself to delete all its children before it deletes the given node.
     *
     * @param LinksAndChildNode $node
     * @throws \Exception
     */
    protected static function deleteNode(LinksAndChildNode $node)
    {
        // Delete/unembed    the child nodes (and their associated data record).
        /** @var LinksAndChildNode $childNode */
        foreach ($node->childNodes()->get() as $childNode) {
            static::deleteNode($childNode);
        }
        // Delete the parent link row.
        self::unembedNode($node);
        // If the node belongs to the user, delete it as well.
        // Note: there is an event handler that deletes the associated record before the node delete is performed.
        if ($node->userId == Auth::id()) {
            Node::find($node->id)->delete();
        }
    }

    protected static function unembedNode(LinksAndChildNode $node)
    {
        // Just remove the link between the parent & child in the object passed in.
        $parentChildLink = NodeParentToChild::where('childNodeId', $node->id);
        if ($node->parentNodeId) {
            $parentChildLink->where('parentNodeId', $node->parentNodeId);
        } else {
            $parentChildLink->whereNull('parentNodeId');
        }
        $parentChildLink->delete();
    }
}