<?php

namespace App\Services;

use App\Models\AbstractNodeType;
use App\Models\Node;
use App\Models\NodeParentToChild;
use App\Models\Page;

class NodeTreeTraverser
{
    /**
     * For the given node, return a tree of its ancestors all the way up to the page.
     * @param Node $node
     * @return array
     */
    public static function getHierarchy(Node $node)
    {
        $result = [
            'node' => $node,
            'parents' => []
        ];

        /** @var NodeParentToChild[] $parentLinks */
        $parentLinks = NodeParentToChild::with(AbstractNodeType::buildWithForAssocRows(['parentNode', 'parentNode.owner'], 'parentNode'))
            ->where('childNodeId', $node->id)
            ->whereNotNull('parentNodeId')
            ->get();
        foreach($parentLinks as $parentLink) {
            $result['parents'][] = self::getHierarchy($parentLink->parentNode);
        }

        return $result;
    }

    public static function getPagesFor(Node $node)
    {
        return self::getPages(self::getHierarchy($node));
    }

    private static function getPages($hierarchy, array $pagesSoFar = [])
    {
        if (isset($hierarchy['parents']) && count($hierarchy['parents'])) {
            foreach($hierarchy['parents'] as $parentNode) {
                $pagesSoFar = self::getPages($parentNode, $pagesSoFar);
            }

            return $pagesSoFar;
        } else {
            /** @var Node $pageNode */
            $pageNode = $hierarchy['node'];
            // We're at the top level. Make sure we're on a page, just in case.
            if (ucwords($pageNode->nodeType) == Page::NODE_TYPE) {
                $pagesSoFar[] = $pageNode;
            }

            return $pagesSoFar;
        }
    }
}