<?php

namespace App\Http\Controllers;

use App\Models\Bookmark;
use App\Models\LinksAndChildNode;
use App\Models\Panel;
use App\Services\Savers\BookmarkSaverFromRequest;
use App\Services\Validators\BookmarkValidatorFromRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class BookmarksController extends AbstractElementController
{
    public function store($panelId, Request $request)
    {
        try {
            // Validate the incoming data.
            BookmarkValidatorFromRequest::validateNew(Panel::NODE_TYPE, $panelId, $request);

            // Try to save the new data.
            $bookmark = BookmarkSaverFromRequest::saveNew(Panel::NODE_TYPE, $panelId, $request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($bookmark);
    }

    public function update(Request $request, int $id)
    {
        try {
            // Validate the new values.
            BookmarkValidatorFromRequest::validateExisting($request/*, $id*/);

            // Save the values to the row(s).
            $bookmark = BookmarkSaverFromRequest::saveExisting($request, $id);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($bookmark);
    }

    protected function getModel(int $id): Model
    {
        return Bookmark::find($id);
    }

    protected function retrieveElement(Model $element): string
    {
        // Load the node & bookmark.
        $result = LinksAndChildNode::with('bookmark')->find($element->nodeId);

        // Return the bookmark as JSON.
        return $result->toJson();
    }
}