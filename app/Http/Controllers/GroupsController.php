<?php

namespace App\Http\Controllers;

use App\Models\AbstractNodeType;
use App\Models\Group;
use App\Models\GroupMember;
use App\Models\GroupMemberRole;
use App\Models\GroupsNode;
use App\Models\User;
use App\Services\EmailSender;
use App\Services\Savers\GroupSaverFromRequest;
use App\Services\Validators\GroupNodeValidator;
use App\Services\Validators\GroupValidatorFromRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GroupsController extends Controller
{
    public function store(Request $request)
    {
        try {
            // Validate the incoming data.
            GroupValidatorFromRequest::validateNew($request);

            // Try to save the new data.
            $newGroup = GroupSaverFromRequest::saveNew($request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->getGroupAsJson($newGroup);
    }

    public function update(Request $request, int $id)
    {
        try {
            // Validate the new values.
            GroupValidatorFromRequest::validateExisting($request, $id);

            // Save the values to the row.
            $group = GroupSaverFromRequest::saveExisting($request, $id);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->getGroupAsJson($group);
    }

    public function show(int $id)
    {
        // Load the given group, making sure the user is a member of that group.
        $isInGroup = GroupValidatorFromRequest::isInGroup($id);
        if (!$isInGroup) {
            throw new \InvalidArgumentException('Group does not exist');
        }

        return Group::with(['members', 'members.user'])->find($id);
    }

    public function destroy(int $groupId)
    {
        try {
            GroupValidatorFromRequest::validateDeleteGroup($groupId);

            GroupSaverFromRequest::delete($groupId);

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function resendInvitation(Request $request, int $groupId, int $userId)
    {
        if (!$request->has('managementUrl')) {
            return response('Could not re-send invitation: management URL was not supplied', 400);
        }

        $user = User::find($userId);
        $group = Group::find($groupId);
        EmailSender::sendMemberInvite($user, $group, $request->get('managementUrl'));

        return response()->json(['success' => true]);
    }

    public function inviteMember(Request $request, int $groupId)
    {
        try {
            // @TODO: All this stuff must be moved into services, like the ones for creating/editing groups.
            // Check their privileges.
            if (!GroupValidatorFromRequest::isInGroup($groupId, true)) {
                throw new \InvalidArgumentException('You do not have admin rights on that group');
            }
            // Find the user.
            $email = $request->get('email');
            if (!$email) {
                throw new \InvalidArgumentException('You must supply an email address for the user');
            }
            /** @var User $user */
            $user = User::where('email', $email)->first();
            if (!$user) {
                throw new \InvalidArgumentException('User not found');
            }
            // Get any existing link record between the member and the group. If there isn't one, we can continue.
            // If there is one, and they declined, we can also continue.
            /** @var GroupMemberRole $existing */
            $existing = GroupMemberRole::where('groupId', $groupId)
                ->where('userId', $user->id)
                ->first();
            if ($existing && ($existing->status == GroupMember::STATUS_INVITED || $existing->status == GroupMember::STATUS_ACCEPTED)) {
                throw new \InvalidArgumentException('User has already been invited in to this group');
            }
            // Create/update the link record as appropriate.
            /** @var GroupMember $groupMember */
            if ($existing) {
                $groupMember = GroupMember::find($existing->id);
            } else {
                $groupMember = new GroupMember();
                $groupMember->groupId = $groupId;
                $groupMember->userId = $user->id;
                $groupMember->role = GroupMember::ROLE_MEMBER;
            }
            $groupMember->status = GroupMember::STATUS_INVITED;
            $groupMember->save();

            // Send an email informing the user that they have been invited. Only do it if the client
            // has supplied a management Url (otherwise we don't know where to link to).
            // @TODO: Maybe if a management URL is not supplied, we still send an email and instead of including the link, we say, "Log in and navigates to Groups"?
            if ($request->has('managementUrl')) {
                $group = Group::find($groupId);
                EmailSender::sendMemberInvite($user, $group, $request->get('managementUrl'));
            }

            // Return the group-member record, with the group & user attached.
            $result = GroupMember::with(['group', 'user'])->find($groupMember->id);

            return response()->json($result);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function removeMember(int $groupId, int $userId)
    {
        try {
            // If the user is trying to remove themself, they must not be an admin; otherwise
            // check that the logged-in user is an admin for the group.
            if ($userId === Auth::id() ? !GroupValidatorFromRequest::isInGroup($groupId, false, true) : !GroupValidatorFromRequest::isInGroup($groupId, true)) {
                throw new \InvalidArgumentException('You do not have admin rights on that group');
            }
            // Load the group/member link. Check that the user being removed is not an admin.
            $groupMember = GroupMember::where('groupId', $groupId)->where('userId', $userId)->first();
            if ($groupMember->role != GroupMember::ROLE_MEMBER) {
                throw new \InvalidArgumentException('Cannot remove an admin from a group');
            }

            GroupValidatorFromRequest::validateRemoveMember($groupId, $userId);

            // Everything looks okay - delete the link record.
            $groupMember->delete();

            return $this->members($groupId);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function updateMember(Request $request, int $groupId, int $userId)
    {
        try {
            // If the member is the logged-in user, and the current status is "invited", then assume they're
            // accepting/declining the invitation.
            $alreadyInGroup = true;
            if ($userId === Auth::id()) {
                /** @var GroupMember $groupMember */
                $groupMember = GroupMember::where('groupId', $groupId)->where('userId', $userId)->first();
                if (empty($groupMember)) {
                    throw new \InvalidArgumentException('Member does not exist in that group');
                }
                $alreadyInGroup = $groupMember->status === GroupMember::STATUS_ACCEPTED;
            }

            if ($alreadyInGroup) {
                // User is already in the group - assume they're just changing the role.
                // Check that the logged-in user is an admin for the group.
                if (!GroupValidatorFromRequest::isInGroup($groupId, true)) {
                    throw new \InvalidArgumentException('You do not have admin rights on that group');
                }
                // Check that the new role was passed in and is a valid value.
                if (!$request->has('role')) {
                    throw new \InvalidArgumentException('You must pass in a role when updating a group member');
                }
                $role = $request->get('role');
                if (!in_array($role, [GroupMember::ROLE_MEMBER, GroupMember::ROLE_ADMIN])) {
                    throw new \InvalidArgumentException('New role is invalid');
                }
                // Load the group/member link. Check that one was found.
                $groupMember = GroupMember::where('groupId', $groupId)->where('userId', $userId)->first();
                if (empty($groupMember)) {
                    throw new \InvalidArgumentException('Member does not exist in that group');
                }
                // There must be at least one (non-pending) admin member in the group.
                // Start a transaction, update the role and save the record, then check there is at least one admin.
                DB::beginTransaction();
                $groupMember->role = $role;
                $groupMember->save();
                $adminMembers = GroupMember::where('groupId', $groupId)
                    ->where('role', GroupMember::ROLE_ADMIN)
                    ->where('status', GroupMember::STATUS_ACCEPTED)
                    ->count();
                if ($adminMembers) {
                    DB::commit();
                } else {
                    DB::rollback();
                    throw new \InvalidArgumentException('A group must have at least one admin member');
                }
            } else {
                // User is only invited - assume they are accepting/declining.
                // Check that the decision was passed in and is a valid value.
                if (!$request->has('status')) {
                    throw new \InvalidArgumentException('You must pass in a decision when accepting/declining an invite');
                }
                $status = $request->get('status');
                if (!in_array($status, [GroupMember::STATUS_ACCEPTED, GroupMember::STATUS_DECLINED])) {
                    throw new \InvalidArgumentException('Decision is invalid');
                }
                // Okay, everything looks okay. Load the group-member row, and update it with the decision.
                $groupMember = GroupMember::where('groupId', $groupId)->where('userId', $userId)->first();
                $groupMember->status = $status;
                $groupMember->save();
            }

            // Return the group-member record, with the group & user attached.
            $result = GroupMember::with(['group', 'user'])->find($groupMember->id);

            return response()->json($result);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function members(int $groupId)
    {
        $members = GroupMember::with(['user', 'group'])
            ->where('groupId', $groupId)
            ->whereIn('status', [GroupMember::STATUS_INVITED, GroupMember::STATUS_ACCEPTED])
            ->get();

        return response()->json($members);
    }

    protected function getGroupAsJson(Group $group)
    {
        // Load the group with its member record(s), and each member's user.
        $fullGroup = Group::with(['members', 'members.user'])->find($group->id);

        return response()->json($fullGroup);
    }

    public function addNode(int $groupId, int $nodeId)
    {
        try {
            $linkRow = GroupNodeValidator::validateAndGetLink($groupId, $nodeId);
            // We don't want the link to be there, so if it is, it's a problem.
            if ($linkRow) {
                throw new \InvalidArgumentException('Node is already in that group');
            }

            // Create the group/node link.
            $groupNode = new GroupsNode();
            $groupNode->groupId = $groupId;
            $groupNode->nodeId = $nodeId;
            $groupNode->save();

            return response()->json(['success' => true]);
        } catch(\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function removeNode(int $groupId, int $nodeId)
    {
        try {
            $linkRow = GroupNodeValidator::validateAndGetLink($groupId, $nodeId);
            // We want the link to be there, so if it's not, it's a problem.
            if (!$linkRow) {
                throw new \InvalidArgumentException('Node is not in that group');
            }

            // Check that if the node is embedded in any pages, that the embedding user can "see" the node
            // through another group.
            GroupNodeValidator::validateDeletion($groupId, $nodeId);

            // Delete the link.
            $linkRow->delete();

            return response()->json(['success' => true]);
        } catch(\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function nodes(int $groupId)
    {
        try {
            $isInGroup = GroupValidatorFromRequest::isInGroup($groupId);
            if (!$isInGroup) {
                throw new \InvalidArgumentException('Group does not exist');
            }

            $nodes = GroupsNode::with(AbstractNodeType::buildWithForAssocRows(['group', 'node', 'node.owner'], 'node'))
                ->where('groupId', $groupId)
                ->get();

            return response()->json($nodes);
        } catch(\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }
}