<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class RecaptchaController extends Controller
{
    public function index(Request $request)
    {
        // Make sure the secret has been set up.
        $secret = env('RECAPTCHA_SECRET');
        if (is_null($secret)) {
            return response('Recaptcha secret has not been set up', 400);
        }

        // Request should contain a "captchaResponse".
        if ($request->has('captchaResponse')) {
            $captchaResponse = $request->get('captchaResponse');

            // Send the POST request with our secret and the captcha response.
            $client = new Client();
            $result = $client
                ->post(
                    'https://www.google.com/recaptcha/api/siteverify',
                    [
                        'form_params' => [
                            'secret' => $secret,
                            'response' => $captchaResponse,
                        ],
                        'headers' => [
                            'Accept' => 'application/json',
                        ],
                    ]
                );

            return response((string) $result->getBody()->getContents());
        } else {
            // No captcha response supplied.
            return response('Recaptcha response is required', 400);
        }
    }
}
