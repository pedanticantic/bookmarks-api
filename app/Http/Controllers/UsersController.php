<?php

namespace App\Http\Controllers;

use App\Models\GroupMember;
use App\Models\GroupMemberRole;
use App\Models\Node;
use App\Models\User;
use App\Services\EmailSender;
use App\Services\Encryptor;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function loggedInUser()
    {
        return response()->json(User::find(Auth::id()));
    }

    public function groups(Request $request)
    {
        // Show all the groups that the logged in user is in
        /** @var Collection $groupMembers */
        $groupMembers = GroupMemberRole::where('userId', Auth::id())
            ->whereIn('status', [GroupMember::STATUS_INVITED, GroupMember::STATUS_ACCEPTED])
            ->orderBy('groupName')
            ->get();

        // Return the group(s) that the logged in user is a member of.
        return $groupMembers->toJson();
    }

    public function update(Request $request)
    {
        // Load the user - we need the encrypted password, and we'll save things back to this record at the end.
        /** @var User $user */
        $user = User::find(Auth::id());

        // Validate the request. The user might be updating different things.
        try {
            if ($request->get('email') || $request->get('newPassword')) {
                // They must send the current password. If they do, it must match the current password.
                if ($request->get('currentPassword')) {
                    if (!password_verify($request->get('currentPassword'), $user->password)) {
                        throw new \InvalidArgumentException('Current password is not correct');
                    }
                } else {
                    throw new \InvalidArgumentException(
                        'Current password must be supplied when changing email or password'
                    );
                }
            }
            // If they are changing their email, it must not match any existing email.
            // And it must be different to their current email!
            if ($request->get('email')) {
                if ($request->get('email') == $user->email) {
                    throw new \InvalidArgumentException('Your email address must be different to your current email address');
                }
                $existing = User::where('email', $request->get('email'))->where('id', '<>', Auth::id())->get();
                if (count($existing)) {
                    throw new \InvalidArgumentException('That email address is in use');
                }
                // Send an email to the old address to say it is being changed.
                EmailSender::newEmailOldAddress($user);
            }

            // Update the user. Encode the password as necessary.
            if ($request->get('email')) {
                $user->email = $request->get('email');
            }
            if ($request->get('newPassword')) {
                $user->password = bcrypt($request->get('newPassword'));
            }
            if ($request->get('name')) {
                $user->name = $request->get('name');
            }
            if ($request->get('familyName')) {
                $user->familyName = $request->get('familyName');
            }
            $user->save();

            // If the user changed their address, send an email to the new address to say it has been changed.
            if ($request->get('email')) {
                EmailSender::newEmailNewAddress($user);
            }

            if ($request->get('newPassword')) {
                EmailSender::passwordChanged($user);
            }

            // If the user changed their profile (currently given name and family name), send them an email.
            if ($request->get('name') || $request->get('familyName')) {
                EmailSender::profileChanged($user);
            }

            // Re-read the user and return it.
            return $this->loggedInUser();
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function requestPasswordReset(Request $request)
    {
        try {
            if (empty($request->get('email'))) {
                throw new \InvalidArgumentException('An email address must be supplied');
            }
            if (empty($request->get('resetPasswordUrl'))) {
                throw new \InvalidArgumentException('A password reset email must be supplied');
            }

            $user = User::where('email', $request->get('email'))->first();
            $expirySeconds = $request->get('expirySeconds') ? (int) $request->get('expirySeconds') : null;
            if ($user) {
                EmailSender::sendForgottenPasswordLink($user, $request->get('resetPasswordUrl'), $expirySeconds);
            }

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function updatePassword(Request $request)
    {
        try {
            /** @var User $user */
            $user = null;
            if ($request->get('email') && $request->get('resetPassword')) {
                // They are resetting their password via the "reset password" facility.
                if (!$request->get('u')) {
                    throw new \InvalidArgumentException('Incorrect parameters passed in password reset request');
                }
                $user = User::where('email', $request->get('email'))->first();
                $expiryTime = $request->get('expiryTime') ? (int) $request->get('expiryTime') : time() + 60*60*24;
                if ($expiryTime < time()) {
                    throw new \InvalidArgumentException('The request has expired');
                }
                if (!$user || $request->get('u') != Encryptor::getForUser($user, $expiryTime)) {
                    throw new \InvalidArgumentException('Invalid parameters passed in password reset request');
                }

                $user->password = bcrypt($request->get('resetPassword'));
                $user->save();
            }

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function destroy()
    {
        // Delete this account.
        try {

            // Validate that the user has no pages and is in no groups.
            $numPages = Node::where('userId', Auth::id())
                ->count();
            if ($numPages > 0) {
                throw new \InvalidArgumentException('Cannot delete - user has pages');
            }
            $numGroups = GroupMember::where('userId', Auth::id())
                ->where('status', GroupMember::STATUS_ACCEPTED)
                ->count();
            if ($numGroups > 0) {
                throw new \InvalidArgumentException('Cannot delete - user is in groups');
            }

            // Start a transaction.
            DB::beginTransaction();

            // Do any tidying. Delete any unanswered or declined invites.
            GroupMember::where('userId', Auth::id())
                ->where('status', GroupMember::STATUS_INVITED)
                ->delete();

            // Delete the user. We need the user model for later.
            $user = User::find(Auth::id());
            $user->delete();

            // Commit the transaction.
            DB::commit();

            // Send them an email to confirm the deletion (after the commit).
            EmailSender::accountDeleted($user);

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }
}