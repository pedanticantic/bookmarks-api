<?php

namespace App\Http\Controllers;

use App\Models\Label;
use Illuminate\Support\Facades\Auth;

class LabelsController extends Controller
{
    public function index()
    {
        // For now, show all labels for the current user.
        $labels = Label::where('userId', Auth::id())
            ->orderBy('name')
            ->get();

        // Return the label(s).
        return $labels->toJson();
    }
}