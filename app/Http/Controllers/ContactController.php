<?php

namespace App\Http\Controllers;

use App\Services\EmailSender;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function store(Request $request)
    {
        // For now, just send the email to me, so that people can't spam other people (you don't need to
        // be logged in nor have an account to send one of these emails).
        try {
            // Make sure all parameters are present and valid.
            if (!$request->has('email') || !$request->has('subject') || !$request->has('details')) {
                throw new \InvalidArgumentException('Current password is not correct');
            }

            EmailSender::sendContactForm($request->get('email'), $request->get('subject'), $request->get('details'));

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }
}