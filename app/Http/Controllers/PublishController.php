<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Services\ElementPublisher;

class PublishController extends Controller
{
    public function publishPage(int $pageId)
    {
        try {
            $elementPublisher = new ElementPublisher(Page::NODE_TYPE, $pageId);

            // Validate the incoming data.
            $elementPublisher->checkCanPublish();

            if (!$elementPublisher->alreadyPublished()) {
                // Publish the page.
                $elementPublisher->publish();
            }
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveToken($pageId);
    }

    public function unpublishPage(int $pageId)
    {
        try {
            $elementPublisher = new ElementPublisher(Page::NODE_TYPE, $pageId);

            // Validate the incoming data.
            $elementPublisher->checkCanPublish();

            if (!$elementPublisher->alreadyPublished()) {
                // Page is not published, so throw an error.
                throw new \InvalidArgumentException('Page is not currently published');
            }

            // Unpublish the page.
            $elementPublisher->unpublish();
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveToken($pageId);
    }

    public function getPageToken(int $pageId)
    {
        try {
            $elementPublisher = new ElementPublisher(Page::NODE_TYPE, $pageId);

            // Validate the incoming data.
            $elementPublisher->checkCanPublish();
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveToken($pageId);
    }

    // @TODO: This should have a new parameter of element type.
    protected function retrieveToken($pageId): string
    {
        // Load the node & page.
        $result = Page::where('pages.id', $pageId)
            ->pluck('token')
            ->first();

        // Return the token and its published state as JSON.
        return json_encode([
            'isPublished' => !!$result,
            'token' => $result
        ]);
    }
}