<?php

namespace App\Http\Controllers;

use App\Models\Cell;
use App\Models\LinksAndChildNode;
use App\Services\Savers\BookmarkSaverFromRequest;
use App\Services\Validators\BookmarkValidatorFromRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CellsController extends AbstractElementController
{
    public function storeBookmark($cellId, Request $request)
    {
        try {
            // Validate the incoming data.
            BookmarkValidatorFromRequest::validateNew(Cell::NODE_TYPE, $cellId, $request);

            // Try to save the new data.
            $cell = BookmarkSaverFromRequest::saveNew(Cell::NODE_TYPE, $cellId, $request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($cell);
    }

    protected function getModel(int $id): Model
    {
        return Cell::find($id);
    }

    protected function retrieveElement(Model $element): string
    {
        // Load the node & cell.
        $result = LinksAndChildNode::with('cell')->find($element->nodeId);

        // Return the cell as JSON.
        return $result->toJson();
    }
}