<?php

namespace App\Http\Controllers;

use App\Models\LinksAndChildNode;
use App\Services\Savers\ColumnSaverFromRequest;
use App\Services\Validators\ColumnValidatorFromRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ColumnsController extends Controller
{
    public function store($gridId, Request $request)
    {
        try {
            // Validate the incoming data.
            ColumnValidatorFromRequest::validateNew($gridId, $request);

            // Try to save the new data.
            $grid = ColumnSaverFromRequest::saveNew($gridId, $request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($grid);
    }

    protected function retrieveElement(Model $element): string
    {
        // Load the node & section.
        $result = LinksAndChildNode::with('grid')->find($element->nodeId);

        // Return the row as JSON.
        return $result->toJson();
    }
}