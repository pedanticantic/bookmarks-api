<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractElementController extends Controller
{
    abstract protected function getModel(int $id): Model;
    // @TODO: I think we can refactor retrieveElement further. That method can be in this class, and the model name referenced dynamically.
    abstract protected function retrieveElement(Model $element): string;
}