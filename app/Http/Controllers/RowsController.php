<?php

namespace App\Http\Controllers;

use App\Models\LinksAndChildNode;
use App\Models\Row;
use App\Services\Savers\RowSaverFromRequest;
use App\Services\Validators\RowValidatorFromRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class RowsController extends AbstractElementController
{
    public function store($gridId, Request $request)
    {
        try {
            // Validate the incoming data.
            RowValidatorFromRequest::validateNew($gridId, $request);

            // Try to save the new data.
            $row = RowSaverFromRequest::saveNew($gridId, $request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($row);
    }

    protected function getModel(int $id): Model
    {
        return Row::find($id);
    }

    protected function retrieveElement(Model $element): string
    {
        // Load the node & section.
        $result = LinksAndChildNode::with('row')->find($element->nodeId);

        // Return the row as JSON.
        return $result->toJson();
    }
}