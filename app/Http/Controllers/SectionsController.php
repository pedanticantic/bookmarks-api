<?php

namespace App\Http\Controllers;

use App\Models\Section;
use App\Models\LinksAndChildNode;
use App\Services\Savers\BookmarkSaverFromRequest;
use App\Services\Savers\SectionSaverFromRequest;
use App\Services\Validators\BookmarkValidatorFromRequest;
use App\Services\Validators\SectionValidatorFromRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class SectionsController extends AbstractElementController
{
    public function storeSection($panelId, Request $request)
    {
        try {
            // Validate the incoming data.
            SectionValidatorFromRequest::validateNew($panelId, $request);

            // Try to save the new data.
            $section = SectionSaverFromRequest::saveNew($panelId, $request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($section);
    }

    public function storeBookmark($sectionId, Request $request)
    {
        try {
            // Validate the incoming data.
            BookmarkValidatorFromRequest::validateNew(Section::NODE_TYPE, $sectionId, $request);

            // Try to save the new data.
            $section = BookmarkSaverFromRequest::saveNew(Section::NODE_TYPE, $sectionId, $request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($section);
    }

    public function update(Request $request, int $id)
    {
        try {
            // Validate the new values.
            SectionValidatorFromRequest::validateExisting($request/*, $id*/);

            // Save the values to the row(s).
            $section = SectionSaverFromRequest::saveExisting($request, $id);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($section);
    }

    protected function getModel(int $id): Model
    {
        return Section::find($id);
    }

    protected function retrieveElement(Model $element): string
    {
        // Load the node & section.
        $result = LinksAndChildNode::with('section')->find($element->nodeId);

        // Return the section as JSON.
        return $result->toJson();
    }
}