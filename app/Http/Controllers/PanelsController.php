<?php

namespace App\Http\Controllers;

use App\Models\LinksAndChildNode;
use App\Models\Page;
use App\Models\Panel;
use App\Services\Savers\PanelSaverFromRequest;
use App\Services\Validators\PanelValidatorFromRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PanelsController extends AbstractElementController
{
    public function show(int $pageId)
    {
        // Find the node for the given page, and throw an error if it can't be found (or user doesn't have access).
        $pageNode = LinksAndChildNode::where('userId', Auth::id())
            ->where('nodeType', Page::NODE_TYPE)
            ->whereHas('page', function ($query) use ($pageId) {
                $query->where('pages.id', $pageId);
            })
            ->first();
        if (!$pageNode) {
            return response('Page does not exist', 400);
        }
        // Now get all the panels in that page, and return them, along with their nodes.
        $panels = LinksAndChildNode::with('panel')
            ->where('nodeType', Panel::NODE_TYPE)
            ->where('parentNodeId', $pageNode->id)
            ->orderBy('sequenceNumber')
            ->get();

        return $panels->toJson();
    }

    public function store($pageId, Request $request)
    {
        try {
            // Validate the incoming data.
            PanelValidatorFromRequest::validateNew($pageId, $request);

            // Try to save the new data.
            $panel = PanelSaverFromRequest::saveNew($pageId, $request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($panel);
    }

    public function update(Request $request, int $id)
    {
        try {
            // Validate the new values.
            PanelValidatorFromRequest::validateExisting($request, $id);

            // Save the values to the row(s).
            $panel = PanelSaverFromRequest::saveExisting($request, $id);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($panel);
    }

    protected function getModel(int $id): Model
    {
        return Panel::find($id);
    }

    protected function retrieveElement(Model $element): string
    {
        // Load the node & panel.
        $result = LinksAndChildNode::with('panel')->find($element->nodeId);

        // Return the panel as JSON.
        return $result->toJson();
    }
}