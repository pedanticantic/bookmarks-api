<?php

namespace App\Http\Controllers;

use App\Models\AbstractNodeType;
use App\Models\GroupMember;
use App\Models\GroupsNode;
use App\Models\Node;
use App\Services\NodeDeleter;
use App\Services\NodeEmbed;
use App\Services\NodeMover;
use App\Services\NodeTreeTraverser;
use App\Services\Validators\NodeDeleteValidator;
use App\Services\Validators\NodeEmbedValidator;
use App\Services\Validators\NodeMoveValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NodeController extends Controller
{
    public function hierarchies(int $nodeId)
    {
        try {
            // This is a bit yucky. We need to start from the given node, and find all its parents, and its parent's
            // parents and so on until we get to a page. We need to return all the intermediate nodes, and there might
            // be a different number of levels to the page, and nodes can have multiple parents, etc.
            $node = Node::with(AbstractNodeType::buildWithForAssocRows(['owner']))
                ->find($nodeId);

            // Check that the node either belongs to the user or is shared in a group that the user has access to.
            if ($node->userId != Auth::id()) {
                // User doesn't own the node.
                // @TODO: This seems a very long-winded way of filtering, but I can't find a better way of doing it.
                $nodeGroupMembers = GroupsNode::with(['group', 'group.members'])
                    ->where('nodeId', $nodeId)
                    ->get()
                    ->filter(function($nodeGroupMember) {
                        return $nodeGroupMember
                            ->group
                            ->members
                            ->where('userId', Auth::id())
                            ->where('status', GroupMember::STATUS_ACCEPTED)
                            ->count() > 0;
                    });
                if ($nodeGroupMembers->count() == 0) {
                    throw new \RuntimeException('You do not have access to that node');
                }
            }

            $nodeHierarchy = NodeTreeTraverser::getHierarchy($node);

            return response()->json($nodeHierarchy);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function delete(int $nodeId, int $parentNodeId = null)
    {
        try {
            // We expect a node that the client has permissions to delete (owns the node and node is in an
            // appropriate state).
            $node = NodeDeleteValidator::validateDelete($nodeId, $parentNodeId);

            $result = NodeDeleter::delete($node);

            return json_encode($result);
        } catch(\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function sharedWith(Request $request)
    {
        // Return all the nodes that are shared with groups that the user is in, where the user doesn't own the
        // node. Ie return all nodes shared with the user.
        // Return a flag that says whether the node is embedded in the (optional) page passed in.
        // @TODO: We need to handle the situation where user A shares a node with a group, user B embeds the node
        // @TODO: in their page, then user A removes the node from their page. User B essentially now owns the node,
        // @TODO: but user A won't be able to see it.
        $nodes = GroupsNode::with(AbstractNodeType::buildWithForAssocRows(['group', 'node', 'node.owner'], 'node'))
            ->join('groups_members', 'groups_nodes.groupId', 'groups_members.groupId')
            ->where('groups_members.userId', Auth::id())
            ->where('groups_members.status', GroupMember::STATUS_ACCEPTED)
            ->whereHas('node', function ($query) {
                $query->where('nodes.userId', '<>', Auth::id()); // @TODO: This is slightly too simplistic - see @TODO above.
            })
            ->get();

        return response()->json($nodes);
    }

    /**
     * Embed the given node in another page. The parent node and sequence number are supplied.
     * @param int $nodeId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|string|\Symfony\Component\HttpFoundation\Response
     */
    public function embed(int $nodeId)
    {
        try {
            $embedInfo = NodeEmbedValidator::validateEmbed($nodeId);
            $result = NodeEmbed::embed($embedInfo);

            return json_encode($result);
        } catch(\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }

    public function move(int $nodeId)
    {
        try {
            // We expect a node (that is being moved), and parent node (where the node is moving to) and
            // a child index (where the node will sit under the new parent) to be passed in.
            $moveInfo = NodeMoveValidator::validateMove($nodeId);

            $result = NodeMover::move($moveInfo);

            return json_encode($result);
        } catch(\Exception $e) {
            return response($e->getMessage(), 400);
        }
    }
}