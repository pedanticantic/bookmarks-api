<?php

namespace App\Http\Controllers;

use App\Models\LinksAndChildNode;
use App\Models\Page;
use App\Services\Savers\PageSaverFromRequest;
use App\Services\Validators\PageValidatorFromRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagesController extends AbstractElementController
{
    public function index(Request $request)
    {
        // For now, show all pages for the current user.
        $pages = LinksAndChildNode::with('page')
            ->where('userId', Auth::id())
            ->where('nodeType', Page::NODE_TYPE)
            ->whereNull('parentNodeId')
            ->orderBy('sequenceNumber')
            ->get();

        // Return the page(s).
        return $pages->toJson();
    }

    public function show(int $pageId)
    {
        $pages = LinksAndChildNode::with('page')
            ->where('userId', Auth::id())
            ->where('nodeType', Page::NODE_TYPE)
            ->whereNull('parentNodeId')
            ->whereHas('page', function (Builder $query) use ($pageId) {
                $query->where('pages.id', $pageId);
            })
            ->first();

        // Return the page(s).
        return $pages->toJson();
    }

    public function getPageByToken(string $token)
    {
        // Note this does not check ownership or do any validation - if there's a page with the token, it is returned.
        $pages = LinksAndChildNode::with('page')
            ->where('nodeType', Page::NODE_TYPE)
            ->whereHas('page', function (Builder $query) use ($token) {
                $query->where('pages.token', $token);
            })
            ->first();

        // Return the page.
        $result = $pages;

        return $result->toJson();
    }

    public function store(Request $request)
    {
        try {
            // Validate the incoming data.
            PageValidatorFromRequest::validateNew($request);

            // Try to save the new data.
            $page = PageSaverFromRequest::saveNew($request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($page);
    }

    public function update(Request $request, int $id)
    {
        try {
            // Validate the new values.
            PageValidatorFromRequest::validateExisting($request, $id);

            // Save the values to the row(s).
            $page = PageSaverFromRequest::saveExisting($request, $id);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($page);
    }

    protected function getModel(int $id): Model
    {
        return Page::find($id);
    }

    protected function retrieveElement(Model $element): string
    {
        // Load the node & page.
        $result = LinksAndChildNode::with('page')->find($element->nodeId);

        // Return the page as JSON.
        return $result->toJson();
    }
}
