<?php

namespace App\Http\Controllers;

use App\Models\Grid;
use App\Models\LinksAndChildNode;
use App\Services\Savers\GridSaverFromRequest;
use App\Services\Validators\GridValidatorFromRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class GridsController extends AbstractElementController
{
    public function store($sectionId, Request $request)
    {
        try {
            // Validate the incoming data.
            GridValidatorFromRequest::validateNew($sectionId, $request);

            // Try to save the new data.
            $section = GridSaverFromRequest::saveNew($sectionId, $request);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return $this->retrieveElement($section);
    }

    protected function getModel(int $id): Model
    {
        return Grid::find($id);
    }

    protected function retrieveElement(Model $element): string
    {
        // Load the node & section.
        $result = LinksAndChildNode::with('grid')->find($element->nodeId);

        // Return the grid as JSON.
        return $result->toJson();
    }
}