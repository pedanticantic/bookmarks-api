<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookmarksLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookmarks_labels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('labelId')->unsigned();
            $table->foreign('labelId')->references('id')->on('labels');
            $table->integer('bookmarkId')->unsigned();
            $table->foreign('bookmarkId')->references('id')->on('bookmarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookmarks_labels');
    }
}
