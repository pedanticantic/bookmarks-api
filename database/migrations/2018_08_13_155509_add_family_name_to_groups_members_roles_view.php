<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddFamilyNameToGroupsMembersRolesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
CREATE OR REPLACE VIEW groups_members_roles AS
SELECT `g1`.`id` AS `groupId`, `g1`.`name` AS `groupName`, `g1`.`createdByUserId`,
       `gm1`.`id`, `gm1`.`role`, `gm1`.`status`,
       `u1`.`id` AS `userId`, `u1`.`name` AS `userGivenName`, `u1`.`familyName` AS `userFamilyName`
FROM   `groups` AS `g1`
INNER JOIN `groups_members` AS `gm1` ON `g1`.`id` = `gm1`.`groupId`
INNER JOIN `users` AS `u1` ON `gm1`.`userId` = `u1`.`id`
');
    }

    /**
     * Reverse the migrations.
     * Revert the view to how it was before the migrtion.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('
CREATE OR REPLACE VIEW groups_members_roles AS
SELECT `g1`.`id` AS `groupId`, `g1`.`name` AS `groupName`, `g1`.`createdByUserId`,
       `gm1`.`id`, `gm1`.`role`, `gm1`.`status`,
       `u1`.`id` AS `userId`, `u1`.`name` AS `userName`
FROM   `groups` AS `g1`
INNER JOIN `groups_members` AS `gm1` ON `g1`.`id` = `gm1`.`groupId`
INNER JOIN `users` AS `u1` ON `gm1`.`userId` = `u1`.`id`
');
    }
}
