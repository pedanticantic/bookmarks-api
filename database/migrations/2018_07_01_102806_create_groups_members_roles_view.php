<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateGroupsMembersRolesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Just a simple view that joins groups, members and users.
        DB::statement('
CREATE VIEW groups_members_roles AS
SELECT `g1`.`id` AS `groupId`, `g1`.`name` AS `groupName`, `g1`.`createdByUserId`,
       `gm1`.`id`, `gm1`.`role`, `gm1`.`status`,
       `u1`.`id` AS `userId`, `u1`.`name` AS `userName`
FROM   `groups` AS `g1`
INNER JOIN `groups_members` AS `gm1` ON `g1`.`id` = `gm1`.`groupId`
INNER JOIN `users` AS `u1` ON `gm1`.`userId` = `u1`.`id`
');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS groups_members_roles');
    }
}
