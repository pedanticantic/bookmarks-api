<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups_members', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('groupId');
            $table->unsignedInteger('userId');
            $table->enum('role', ['Admin', 'Member']);
            $table->enum('status', ['Invited', 'Accepted', 'Declined']);
            $table->timestamps();
            $table->foreign('groupId')->references('id')->on('groups');
            $table->foreign('userId')->references('id')->on('users');
            $table->unique(['groupId', 'userId']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups_members');
    }
}
