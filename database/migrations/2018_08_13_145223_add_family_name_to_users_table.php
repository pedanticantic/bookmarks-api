<?php

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFamilyNameToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // We need to add the "family name" column, backfill it with something that says, "update me",
        // then make it mandatory.
        Schema::table('users', function (Blueprint $table) {
            $table
                ->string('familyName')
                ->nullable()
                ->after('name');
        });
        DB::table('users')
            ->update(['familyName' => '[Please change me]']);
        Schema::table('users', function (Blueprint $table) {
            $table
                ->string('familyName')
                ->nullable(false)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Just drop the "family name" column.
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('familyName');
        });
    }
}
