<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropObsoleteColumnsFromNodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Drop parent node id and sequence number from nodes.
        // I'm assuming nothing has added rows to the new parent-child nodes table.
        Schema::table('nodes', function (Blueprint $table) {
            $table->dropForeign(['parentNodeId']);
            $table->dropColumn(['parentNodeId', 'sequenceNumber']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Add the columns back in as nullable, but with their FKs.
        Schema::table('nodes', function (Blueprint $table) {
            $table->integer('parentNodeId')->nullable()->unsigned()->after('id');
            $table->foreign('parentNodeId')->references('id')->on('nodes');
            $table->integer('sequenceNumber')->nullable()->after('nodeType');
        });

        // Backfill them from the parent-to-child table.
        DB::statement('
UPDATE `nodes` AS `n1`
INNER JOIN `node_parent_to_child` AS `npc1` ON `n1`.`id` = `npc1`.`childNodeId`
SET    `n1`.`parentNodeId` = `npc1`.`parentNodeId`,
       `n1`.`sequenceNumber` = `npc1`.`sequenceNumber`
');

        // Make sequenceNumber mandatory.
        DB::statement('
ALTER TABLE `nodes`
MODIFY `sequenceNumber` int(11) NOT NULL
');
    }
}
