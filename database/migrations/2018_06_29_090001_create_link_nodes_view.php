<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateLinkNodesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the view on the node-node link table and the *child* node.
        // It's like this because pages have a "parent" link but no parent node, and the bottom-level nodes
        // have no child link.
        DB::statement('
CREATE VIEW links_and_child_nodes AS
SELECT `n1`.`id`,
`n1`.`userId`,
`n1`.`nodeType`,
`n1`.`created_at`,
`n1`.`updated_at`,
`p2c`.`userId` AS `linkUserId`,
`p2c`.`parentNodeId`,
`p2c`.`sequenceNumber`
FROM   `node_parent_to_child` AS `p2c`
INNER JOIN `nodes` AS `n1` ON `p2c`.`childNodeId` = `n1`.`id`
');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop the view between node links and nodes.
        DB::statement('DROP VIEW IF EXISTS links_and_child_nodes');
    }
}
