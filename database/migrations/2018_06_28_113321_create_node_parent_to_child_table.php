<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodeParentToChildTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('node_parent_to_child', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('userId');
            $table->unsignedInteger('parentNodeId')->nullable();
            $table->unsignedInteger('childNodeId');
            $table->integer('sequenceNumber');
            $table->timestamps();
            $table->foreign('userId')->references('id')->on('users');
            $table->foreign('parentNodeId')->references('id')->on('nodes');
            $table->foreign('childNodeId')->references('id')->on('nodes');
        });

        DB::statement('
INSERT INTO `node_parent_to_child`(`userId`, `parentNodeId`, `childNodeId`, `sequenceNumber`)
SELECT `n1`.`userId`, `n1`.`parentNodeId`, `n1`.`id`, `n1`.`sequenceNumber`
FROM   `nodes` AS `n1`
ORDER BY `n1`.`id`
');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('node_parent_to_child');
    }
}
