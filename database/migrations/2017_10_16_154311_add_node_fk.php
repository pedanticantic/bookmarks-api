<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNodeFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->integer('nodeId')->unsigned()->after('id');
            $table->foreign('nodeId')->references('id')->on('nodes');
        });
        Schema::table('panels', function (Blueprint $table) {
            $table->integer('nodeId')->unsigned()->after('id');
            $table->foreign('nodeId')->references('id')->on('nodes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
