# bookmarks-api
API for the bookmarks application

There is a client application [here](https://github.com/pedanticantic/bookmarks-client).

## Installing
Decide on a host name for the API, and if you want the client app, a host name for that. In this document, I'll assume `api.bookmarks` and `client.bookmarks`.

Git clone the API repo.

I will assume you have the necessary packages installed, etc - web server, database server, composer, etc.

Note: you'll probably have a lot of problems with some or all of the following, which are beyond the scope of this readme, so you'll need to sort them out yourself as they happen.
- old or not installed packages
- file permissions
- possibly other things

Please let me know if I have made any mistakes, missed anything, or if anything can be improved.

### Create the database and a user
I'm assuming a DB called `bk_db` and a user `bk_user`. These are deliberately bad names; use better names in your installation.
```
CREATE DATABASE bk_db;
CREATE USER 'bk_user'@'localhost' IDENTIFIED BY 'bk_pass';
GRANT ALL ON bk_db.* TO 'bk_user'@'localhost';
```

### Configure the credentials in the app
Copy the `.env.example` file to `.env`

Edit `.env` and enter the DB credentials you created above, where relevant.

### Install all the PHP packages
`composer install`

### Run database migrations
`php artisan migrate`

### Configure the web server
Basically serve pages from the `public` folder of your repo, for the domain you chose (eg `app.bookmarks`).

Configure CORS, if necessary.

### Configure the application
Note: I am not absolutely sure of the order here, or even if all of the commands are needed, as I've only done it once, and had lots of problems, so I was going backwards and forwards a lot. You definitely need `php artisan passport:install`.
```
php artisan passport:keys
php artisan key:generate
php artisan passport:install
```

You'll get output something like this:
```
Encryption keys generated successfully.
Personal access client created successfully.
Client ID: 1
Client Secret: 55d74d57dw99fh....7665r6rd7s6t7f6r
Password grant client created successfully.
Client ID: 2
Client Secret: w8yerf87w47yr9....87yt87fh98y3gh9w
```

If you are using the client app, you will need to put the 2nd Client ID and Client secret (for Password grant) into the client app config. See the readme in that repo for more details.
